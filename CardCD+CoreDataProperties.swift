//
//  CardCD+CoreDataProperties.swift
//  arena_forge
//
//  Created by Paul Weston on 28/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import Foundation
import CoreData


extension CardCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CardCD> {
        return NSFetchRequest<CardCD>(entityName: "CardCD")
    }

    @NSManaged public var name: String?
    @NSManaged public var attack: Int16
    @NSManaged public var cost: Int16
    @NSManaged public var health: Int16
    @NSManaged public var cardClass: String?
    @NSManaged public var id: Int32
    @NSManaged public var text: String?
    @NSManaged public var flavor: String?
    @NSManaged public var rarity: String?
    @NSManaged public var type: String?
    @NSManaged public var imageURL: String?

}
