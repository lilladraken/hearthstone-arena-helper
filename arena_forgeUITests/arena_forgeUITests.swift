//
//  arena_forgeUITests.swift
//  arena_forgeUITests
//
//  Created by Steph Ridnell on 27/12/17.
//  Copyright © 2017 Taylor Swift. All rights reserved.
//

import XCTest

class arena_forgeUITests: XCTestCase {
        
  override func setUp() {
    super.setUp()
        
    // Put setup code here. This method is called before the invocation of each test method in the class.
    let app = XCUIApplication()
    
    // Flag to the app to create a test user profile
    app.launchArguments = ["TestProfile"]
    
      
    // In UI tests it is usually best to stop immediately when a failure occurs.
    continueAfterFailure = false
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    app.launch()

    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
  }
    
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func testUI() {
    
    // Setup Date Formatter & Current Date String
    let date = Date()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MM-YY"
    let formattedDate = dateFormatter.string(from: date)

    // App Reference
    let app = XCUIApplication()
    
    // Condition for detecting when the  the activity indicator is active
    // No telling if it will be the first time the app is opened or not
    let predicate = NSPredicate(format: "count != 1")
    let query = app.activityIndicators
    expectation(for: predicate, evaluatedWith: query, handler: nil)
    waitForExpectations(timeout: 100, handler: nil)
    
    // Assert - Check Images, Labels, TextFields and Buttons
    XCTAssertEqual(app.images.count, 2)
    XCTAssertEqual(app.textFields.count, 1)
    XCTAssertEqual(app.secureTextFields.count, 1)
    XCTAssertEqual(app.staticTexts.count, 1)
    XCTAssertEqual(app.buttons.count, 2)
    XCTAssert(app.buttons["NEXT"].exists)
    XCTAssert(app.buttons["Create Profile"].exists)
    
    
    // Action - Change to the Create Profile Scene
    app.buttons["Create Profile"].tap()
    
    // Action - Create Test Profile
    let usernameTextField = app.textFields["Username"]
    usernameTextField.tap()
    usernameTextField.typeText("user")
    app.images["logo"].tap()
    
    let passwordSecureTextField = app.secureTextFields["Password"]
    passwordSecureTextField.tap()
    passwordSecureTextField.typeText("pass")
    app.images["logo"].tap()
    app.buttons["CREATE"].tap()
    app.tabBars.buttons["Profile"].tap()
    
    // Action - Logout
    app.navigationBars["Profile"].buttons["ic exit to app white"].tap()
    app.sheets["Log Out"].buttons["Log Out"].tap()
    
    
    // Action - Enter Username and Password
    usernameTextField.tap()
    usernameTextField.typeText("user")
    
    passwordSecureTextField.tap()
    passwordSecureTextField.typeText("pass")
    app.images["logo"].tap()  
    app.buttons["NEXT"].tap()
    
    // Action - Change to the Profile Tab and Edit User Information
    let tabBarsQuery = app.tabBars
    tabBarsQuery.buttons["Profile"].tap()
    
    // Assert - Check Images, Labels, TextFields and Buttons
    XCTAssertEqual(app.images.count, 2)
    XCTAssertEqual(app.textFields.count, 2)
    XCTAssertEqual(app.secureTextFields.count, 0)
    XCTAssertEqual(app.staticTexts.count, 4)
    XCTAssertEqual(app.collectionViews.cells.count, 0)
    XCTAssertEqual(app.buttons.count, 6)
    XCTAssert(app.staticTexts["Username"].exists)
    XCTAssert(app.staticTexts["Email"].exists)
    XCTAssert(app.staticTexts["Favourite Hero"].exists)
    
    let elementsQuery = app.scrollViews.otherElements
    let editUsernameTextField = elementsQuery.textFields["Edit Username"]
    editUsernameTextField.tap()
    editUsernameTextField.typeText("TestL33T")
    elementsQuery.staticTexts["Username"].tap()
    
    let editEmailTextField = elementsQuery.textFields["Edit Email"]
    editEmailTextField.tap()
    editEmailTextField.typeText("email@email.com")
    elementsQuery.staticTexts["Username"].tap()
    elementsQuery.buttons["Select Hero"].tap()
    
    // Assert - Check Images, Labels, TextFields, Table View Cells and Buttons
    XCTAssertEqual(app.images.count, 0)
    XCTAssertEqual(app.textFields.count, 0)
    XCTAssertEqual(app.secureTextFields.count, 0)
    XCTAssertEqual(app.staticTexts.count, 10)
    XCTAssertEqual(app.tables.cells.count, 9)
    XCTAssertEqual(app.buttons.count, 5)
    
    let tablesQuery2 = app.tables
    let tablesQuery = tablesQuery2
    tablesQuery.staticTexts["Thrall"].tap()
    
    
    // Action - Switch to the Arena Tab
    app.tabBars.buttons["Arena"].tap()
    
    // Assert - Check Images, Labels, TextFields, Collection View Cells and Buttons
    XCTAssertEqual(app.images.count, 10)
    XCTAssertEqual(app.textFields.count, 0)
    XCTAssertEqual(app.secureTextFields.count, 0)
    XCTAssertEqual(app.staticTexts.count, 1)
    XCTAssertEqual(app.collectionViews.cells.count, 9)
    XCTAssertEqual(app.buttons.count, 4)
    
    app.collectionViews.cells.otherElements.containing(.image, identifier:"hero-shaman").element.tap()
    app.buttons["NEXT"].tap()
    
    // Assert - Check Images, Labels, TextFields, Collection View Cells and Buttons
    XCTAssertEqual(app.images.count, 8)
    XCTAssertEqual(app.textFields.count, 0)
    XCTAssertEqual(app.secureTextFields.count, 0)
    XCTAssertEqual(app.staticTexts.count, 1)
    XCTAssertEqual(app.collectionViews.cells.count, 0)
    XCTAssertEqual(app.buttons.count, 11)
    
    // Action - Select First Card
    let element2 = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
    let element = element2.children(matching: .other).element(boundBy: 1)
    element.buttons["SEARCH"].tap()
    let searchSearchField = tablesQuery2.searchFields["Search"]
    searchSearchField.tap()
    
    let searchSearchField2 = app.searchFields["Search"]
    searchSearchField2.typeText("Blood")
    tablesQuery.staticTexts["Bloodlust"].tap()
    
    // Assert - Check Images, Labels, TextFields, Collection View Cells and Buttons
    XCTAssertEqual(app.textFields.count, 0)
    XCTAssertEqual(app.secureTextFields.count, 0)
    XCTAssertEqual(app.staticTexts.count, 6)
    XCTAssertEqual(app.collectionViews.cells.count, 0)
    XCTAssertEqual(app.buttons.count, 12)
    XCTAssertEqual(app.images.count, 11)
    
    element2.children(matching: .other).element(boundBy: 2).buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Twil")
    tablesQuery.staticTexts["Twilight Geomancer"].tap()
    
    // Assert - Check Images, Labels, TextFields, Collection View Cells and Buttons
    XCTAssertEqual(app.textFields.count, 0)
    XCTAssertEqual(app.secureTextFields.count, 0)
    XCTAssertEqual(app.staticTexts.count, 11)
    XCTAssertEqual(app.collectionViews.cells.count, 0)
    XCTAssertEqual(app.buttons.count, 13)
    XCTAssertEqual(app.images.count, 14)
    
    element2.children(matching: .other).element(boundBy: 3).buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("C'")
    tablesQuery.staticTexts["C'Thun's Chosen"].tap()
    
    // Assert - Check Images, Labels, TextFields, Collection View Cells and Buttons
    XCTAssertEqual(app.textFields.count, 0)
    XCTAssertEqual(app.secureTextFields.count, 0)
    XCTAssertEqual(app.staticTexts.count, 16)
    XCTAssertEqual(app.collectionViews.cells.count, 0)
    XCTAssertEqual(app.buttons.count, 14)
    XCTAssertEqual(app.images.count, 17)
    
    element.buttons["ic add white"].tap()
    
    // Assert - Check Images, Labels, TextFields, Collection View Cells and Buttons
    XCTAssertEqual(app.images.count, 8)
    XCTAssertEqual(app.textFields.count, 0)
    XCTAssertEqual(app.secureTextFields.count, 0)
    XCTAssertEqual(app.staticTexts.count, 1)
    XCTAssertEqual(app.collectionViews.cells.count, 0)
    XCTAssertEqual(app.buttons.count, 11)
    
    // Action - Select Second Card
    element.buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Zealous Initiate")
    tablesQuery.staticTexts["Zealous Initiate"].tap()
    element2.children(matching: .other).element(boundBy: 2).buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Lightning Bolt")
    tablesQuery.staticTexts["Lightning Bolt"].tap()
    element2.children(matching: .other).element(boundBy: 3).buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Mad Bomber")
    tablesQuery.staticTexts["Mad Bomber"].tap()
    element2.children(matching: .other).element(boundBy: 3).buttons["ic add white"].tap()

    // Action - Select Third Card
    element.buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Imp Master")
    tablesQuery.staticTexts["Imp Master"].tap()
    element2.children(matching: .other).element(boundBy: 2).buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Kabal Courier")
    tablesQuery.staticTexts["Kabal Courier"].tap()
    element2.children(matching: .other).element(boundBy: 3).buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Lightwarden")
    tablesQuery.staticTexts["Lightwarden"].tap()
    element.buttons["ic add white"].tap()
    
    // Action - Select 4th Card
    element.buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Boulderfist")
    tablesQuery.staticTexts["Boulderfist Ogre"].tap()
    element2.children(matching: .other).element(boundBy: 2).buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Bloodfen Raptor")
    tablesQuery.staticTexts["Bloodfen Raptor"].tap()
    element2.children(matching: .other).element(boundBy: 3).buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Nightblade")
    tablesQuery.staticTexts["Nightblade"].tap()
    element.buttons["ic add white"].tap()
    
    // Action - Select 5th Card
    element.buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Rockpool Hunter")
    tablesQuery.staticTexts["Rockpool Hunter"].tap()
    element2.children(matching: .other).element(boundBy: 2).buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Arcane Anomaly")
    tablesQuery.staticTexts["Arcane Anomaly"].tap()
    element2.children(matching: .other).element(boundBy: 3).buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Primal Fusion")
    tablesQuery.staticTexts["Primal Fusion"].tap()
    element2.children(matching: .other).element(boundBy: 3).buttons["ic add white"].tap()
    
    // Action - Open side bar menu and view Mana Curve
    app.navigationBars["Arena"].buttons["ic menu white 36pt"].tap()
    app.buttons["Mana Curve"].tap()
    
    // Assert - Check Images, Labels, TextFields, Collection View Cells and Buttons
    XCTAssertEqual(app.images.count, 9)
    XCTAssertEqual(app.textFields.count, 0)
    XCTAssertEqual(app.secureTextFields.count, 0)
    XCTAssertEqual(app.staticTexts.count, 31)
    XCTAssertEqual(app.collectionViews.cells.count, 0)
    XCTAssertEqual(app.buttons.count, 5)
    
    app.navigationBars["Mana Curve"].buttons["Arena"].tap()
    
    // Action - Save Deck
    app.buttons["Save"].tap()
    let saveDeckAlert = app.alerts["Save Deck"]
    saveDeckAlert.collectionViews.textFields["Shaman \(formattedDate)"].typeText("Hello")
    saveDeckAlert.buttons["OK"].tap()
    
    // Action - Start New Deck
    app.collectionViews.cells.otherElements.containing(.image, identifier:"hero-shaman").element.tap()
    app.buttons["NEXT"].tap()
    element.buttons["SEARCH"].tap()
    
    //Action - Add First Card
    searchSearchField.tap()
    searchSearchField2.typeText("Blood")
    tablesQuery.staticTexts["Bloodlust"].tap()
    element2.children(matching: .other).element(boundBy: 2).buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Twil")
    tablesQuery.staticTexts["Twilight Geomancer"].tap()
    element2.children(matching: .other).element(boundBy: 3).buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("C'")
    tablesQuery.staticTexts["C'Thun's Chosen"].tap()
    element.buttons["ic add white"].tap()

    // Action - Open side bar menu and save deck
    app.navigationBars["Arena"].buttons["ic menu white 36pt"].tap()
    app.buttons["Save"].tap()
    saveDeckAlert.collectionViews.textFields["Shaman \(formattedDate)"].typeText("World")
    saveDeckAlert.buttons["OK"].tap()
    
    // Action - Switch to history tab
    app.tabBars.buttons["History"].tap()
    
    // Action - Select "Hello" Deck
    app.tables.children(matching: .cell).element(boundBy: app.tables.cells.count - 2).staticTexts["Hello"].tap()
    
    // Assert - Check Images, Labels, TextFields, Table Cells and Buttons
    XCTAssertEqual(app.images.count, 1)
    XCTAssertEqual(app.textFields.count, 0)
    XCTAssertEqual(app.secureTextFields.count, 0)
    XCTAssertEqual(app.staticTexts.count, 21)
    XCTAssertEqual(app.collectionViews.cells.count, 0)
    XCTAssertEqual(app.buttons.count, 6)

    
    // Action - Select "Imp Master" Card
    tablesQuery.staticTexts["Imp Master"].tap()
    app.navigationBars["Imp Master"].buttons["Hello"].tap()
    
    // Assert - Check Images, Labels, TextFields, Collection View Cells and Buttons
    XCTAssertEqual(app.images.count, 1)
    XCTAssertEqual(app.textFields.count, 0)
    XCTAssertEqual(app.secureTextFields.count, 0)
    XCTAssertEqual(app.staticTexts.count, 21)
    XCTAssertEqual(app.collectionViews.cells.count, 0)
    XCTAssertEqual(app.buttons.count, 6)
    
    // Action - Continue Adding to Hello Deck
    app.navigationBars["Hello"].buttons["arena"].tap()
    element.buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Blood")
    tablesQuery.staticTexts["Bloodlust"].tap()
    element2.children(matching: .other).element(boundBy: 2).buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("Twil")
    tablesQuery.staticTexts["Twilight Geomancer"].tap()
    element2.children(matching: .other).element(boundBy: 3).buttons["SEARCH"].tap()
    searchSearchField.tap()
    searchSearchField2.typeText("C'")
    tablesQuery.staticTexts["C'Thun's Chosen"].tap()
    element.buttons["ic add white"].tap()

    // Reset to Hero Selection
    app.navigationBars["Arena"].buttons["ic menu white 36pt"].tap()
    app.buttons["View Deck: (6/30)"].tap()
    app.navigationBars["Hello"].buttons["Arena"].tap()
    app.buttons["Restart"].tap()
    app.sheets["Restart Arena Selection"].buttons["Reset"].tap()
    app.tabBars.buttons["History"].tap()
    
    let countBeforeDelete = app.tables.cells.count
    
    // Delete "World" Deck
    let worldStaticText = app.tables.children(matching: .cell).element(boundBy: app.tables.cells.count - 1).staticTexts["World"]
    worldStaticText.swipeLeft()
    app.tables.buttons["Delete"].tap()
    
    let countAfterDeletePlusOne = app.tables.cells.count + 1
    
    // Assert - correct amount of table cells
    XCTAssertEqual(countAfterDeletePlusOne, countBeforeDelete)
    

  }
  
  
    
}
