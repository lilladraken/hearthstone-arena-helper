//
//  arena_forgeTests.swift
//  arena_forgeTests
//
//  Created by Steph Ridnell on 27/12/17.
//  Copyright © 2017 Taylor Swift. All rights reserved.
//

import XCTest
@testable import arena_forge

class arena_forgeTests: XCTestCase {
    
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
    
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }

  //  Hero.swift Unit Test
  func testHero(){
    let warrior = Hero()
    let shaman = Hero(number: 2)
    
    // Test Rank Number
    XCTAssertEqual(warrior.rank, 1)
    XCTAssertEqual(shaman?.rank, 2)
    
    // Test Class Name
    XCTAssertEqual(warrior.className, "Warrior")
    XCTAssertEqual(shaman?.className, "Shaman")
    
    // Test Hero Name
    XCTAssertEqual(warrior.heroName, "Garrosh Hellscream")
    XCTAssertEqual(shaman?.heroName, "Thrall")
    
    // Test Image Name
    XCTAssertEqual(warrior.imageName, "hero-warrior")
    XCTAssertEqual(shaman?.imageName, "hero-shaman")
    
    // Test Number of Heroes
    XCTAssertEqual(Hero.getHeroList().count, 9)
    
  }
  
  // Card.swift Unit Test
  func testCard(){
    
    Card.sharedInstance.saveCard(id: 559,
                                 name: "Leeroy Jenkins",
                                 attack: 6,
                                 cost: 5,
                                 health: 2,
                                 cardClass: "Neutral",
                                 text: "<b>Charge</b>. <b>Battlecry:</b> Summon two 1/1 Whelps for your opponent.",
                                 flavor: "At least he has Angry Chicken.",
                                 rarity: "Legendary",
                                 type: "Minion",
                                 imageURL: "http://media.services.zam.com/v1/media/byName/hs/cards/enus/EX1_116.png",
                                 existing:  Card.sharedInstance.getCardById(id: 559))
    
    let card = Card.sharedInstance.getCardById(id: 559)
    
    // Check Calculated Rank
    XCTAssertEqual(card?.getRank(heroClass: "Warrior"), 48)
    
    // Check Text
    XCTAssertEqual(card?.getText(), "<b>Charge</b>. <b>Battlecry:</b> Summon two 1/1 Whelps for your opponent.")
    
    // Check Flavor
    XCTAssertEqual(card?.getFlavor(), "At least he has Angry Chicken.")
    
    // Check Type
    XCTAssertEqual(card?.getType(), "Minion")
  
  }
  
  // Rank.swift Unit Test
  func testRank(){
  
    let rank = Rank.getRankByName(name: "Leeroy Jenkins")
    
    // Check Rank Values
    XCTAssertEqual(rank?.druidRank,48)
    XCTAssertEqual(rank?.hunterRank,69)
    XCTAssertEqual(rank?.warlockRank,48)
    XCTAssertEqual(rank?.shamanRank,48)
    XCTAssertEqual(rank?.rogueRank,54)
    XCTAssertEqual(rank?.priestRank,47)
    XCTAssertEqual(rank?.paladinRank,48)
    XCTAssertEqual(rank?.warriorRank,48)
    XCTAssertEqual(rank?.mageRank,48)
    
  }
  
  // Deck.swift Unit Test
  func testDeck(){
  
    let deck = Deck.sharedInstance.saveDeck(deckName: "Hello World", deckHeroClass: "Warrior", cardIDs: [], existing: nil)
    
    // Check Number of Cards
    XCTAssertEqual(deck?.getCardIds().count,0)
    
    // Add A Card
    deck?.addCard(id: 559)
    
    // Check Number Of Cards and that the added card is correct
    XCTAssertEqual(deck?.getCardIds().count,1)
    XCTAssertEqual(deck?.getCardIds().count,deck?.cardCount)
    XCTAssertEqual(deck?.getCardIds()[0],559)
    XCTAssertEqual(deck?.isDeckFull,false)
    
    // Check Deck Name
    XCTAssertEqual(deck?.deckName,"Hello World")
    
    // Change Deck Name
    deck?.setName("World Hello")
    
    // Check New Deck Name
    XCTAssertEqual(deck?.deckName,"World Hello")
    
    // Delete Deck
    Deck.sharedInstance.deleteDeck(deck: deck!)
    
  }
  
  // Profile.swift Unit Test
  func testProfile(){
  
    // Create User Profile
    Profile.sharedInstance.saveProfile(userName: "UnitTester", email: "", password: "pass", favHero: "", image: nil, existing: Profile.sharedInstance.getProfileByUsername(username: "UnitTester"))
    
    // Get test user profile
    let profile = Profile.sharedInstance.getProfileByUsername(username: "UnitTester")
    
    // Check User details were loaded correctly by saveProfile function
    XCTAssert(Profile.sharedInstance.doesUsernameExist(username: "UnitTester"))
    XCTAssertEqual(profile?.userName, "UnitTester")
    XCTAssertEqual(profile?.password, "pass")
    
    // Precheck of Email and Favourite Hero Change
    XCTAssertNotEqual(profile?.email, "email@email.com")
    XCTAssertNotEqual(profile?.favHero, "Thrall")
    
    Profile.sharedInstance.saveProfile(userName: "UnitTester", email: "email@email.com", password: "pass", favHero: "Thrall", image: nil, existing: profile!)
    
    // Confirm Email and Favourite Hero Change
    XCTAssertEqual(profile?.email, "email@email.com")
    XCTAssertEqual(profile?.favHero, "Thrall")
    
    // Delete Profile
    Profile.sharedInstance.deleteUser(user: profile!)
    
  }
  
  func testPerformanceExample() {
    // This is an example of a performance test case.
    self.measure {
      // Put the code you want to measure the time of here.
    }
  }
    
}
