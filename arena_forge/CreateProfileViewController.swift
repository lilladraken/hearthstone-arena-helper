//
//  CreateProfileViewController.swift
//  arena_forge
//
//  Created by Paul Weston on 6/2/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import UIKit

class CreateProfileViewController: UIViewController {

  @IBOutlet weak var passwordField: UITextField!
  @IBOutlet weak var usernameField: UITextField!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()

    // Keyboard Dismissal when tapping outside keyboard
    self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view,
                                                          action: #selector(UIView.endEditing(_:))))
  }

  @IBAction func createButtonTapped(_ sender: Any) {
    if usernameField.text != ""{
      if Profile.sharedInstance.doesUsernameExist(username: usernameField.text!){
        profileExistsAlert()
      }
      else{
        Profile.sharedInstance.saveProfile(userName: usernameField.text!, email: "", password: passwordField.text!, favHero: "", image: nil, existing: nil)
        if let userProfile = Profile.sharedInstance.getProfileByUsername(username: usernameField.text!){
          Profile.sharedInstance.setCurrentUserProfile(user: userProfile)
          performSegue(withIdentifier: "createProfile", sender: nil)          
        }
      }
    }
    else{
      blankFieldAlert()
    }
  }
  
  // Alert for when the wrong user information is entered
  func profileExistsAlert(){
    
    let alert = UIAlertController(title: "Username Already Exists", message: "oh no, someone else has that username, sorry for your loss", preferredStyle: UIAlertControllerStyle.alert)
    
    let cancelAction = UIAlertAction(title: "Try Again",
                                     style: .cancel,
                                     handler: nil)
    
    alert.addAction(cancelAction)
    self.present(alert, animated: true)
  }
  
  // Alert for when the user information is left blank
  func blankFieldAlert(){
    
    let alert = UIAlertController(title: "Profile Creation Failed", message: "I'm going to need a little more information", preferredStyle: UIAlertControllerStyle.alert)
    
    let cancelAction = UIAlertAction(title: "Try Again",
                                     style: .cancel,
                                     handler: nil)
    
    alert.addAction(cancelAction)
    self.present(alert, animated: true)
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
  }


}
