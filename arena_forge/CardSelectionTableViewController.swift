//
//  CardSelectionTableViewController.swift
//  arena_forge
//
//  Created by Paul Weston on 6/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import UIKit

class CardSelectionTableViewController: UITableViewController, UISearchResultsUpdating {
  
  // CurrentClass of the Deck from the Arena Controller
  var currentClass:String = "Mage"
  
  // Card Lists
  var availableCards:[CardCD]?
  var filteredCards:[CardCD]?
  
  // Selected Card
  var selectedCard: CardCD?
  
  // Search controller
  let searchController = UISearchController(searchResultsController: nil)
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    availableCards = Card.sharedInstance.getCardsByClass(className: currentClass)! +
                     Card.sharedInstance.getCardsByClass(className: "NEUTRAL")!
    availableCards?.sort{$0.name! < $1.name!}
    filteredCards = availableCards
    
    // Sets up this class as the Search controller and creates the search bar
    // at the top of the table view.
    searchController.searchResultsUpdater = self
    searchController.dimsBackgroundDuringPresentation = false
    tableView.tableHeaderView = searchController.searchBar
    
    self.tabBarController?.delegate = self
    
  }
  
  // Update Table when returning to History View
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    if !Card.sharedInstance.allCards.isEmpty{
      availableCards = Card.sharedInstance.getCardsByClass(className: currentClass)! +
        Card.sharedInstance.getCardsByClass(className: "NEUTRAL")!
      availableCards?.sort{$0.name! < $1.name!}
      filteredCards = availableCards
    }
    tableView.reloadData()
    
    // Refresh Tabbar delegate
    self.tabBarController?.delegate = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    // Deallocate the search controller to avoid xcode warnings
    self.searchController.view.removeFromSuperview()
    self.searchController.isActive = false
  }
  
  // Detemine number of rows
  // We only have one section - so do not need to use the variable 'section'
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if searchController.isActive && searchController.searchBar.text != "" {
      return filteredCards!.count
    }
    return availableCards!.count
  }
  
  //Setup Table cells
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cardSelectionCell", for: indexPath)
    
    // Look up the data item that corresponds to the indexPath.
    let cardIndex: Dictionary = changeDataSource(indexPath: indexPath as NSIndexPath)
    let card = cardIndex["card"] as! CardCD
    
    //Set cell label
    cell.textLabel?.text = card.name!
    cell.textLabel?.textColor = UIColor.white
    
    return cell
  }
  
  // Check to see if we should be displaying data from the filtered list or the main
  // data source.
  func changeDataSource( indexPath: NSIndexPath) -> Dictionary<String, Any> {
    var card: Dictionary<String, Any>
    if searchController.isActive && searchController.searchBar.text != "" {
      card = ["card" : filteredCards![indexPath.row]]
    } else {
      card = ["card" : availableCards![indexPath.row]]
    }
    return card
  }
  
  
  // Method implemented by the protocol that is called when the search bar is being used
  // for searching.
  
  func updateSearchResults(for: UISearchController) {
    filterContentForSearchText(searchText: searchController.searchBar.text!)
  }
  
  // Searches the main data source for the search text to return a filtered result.
  func filterContentForSearchText(searchText: String, scope: String = "All") {
    filteredCards = availableCards?.filter { card in
      return (card.name!.lowercased().contains(searchText.lowercased()))
    }
    
    // reload the table data to display the filtered results.
    tableView.reloadData()
  }
  
  
  
  //Set the selectedCard so that it can be referenced during the unwind
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let indexPath = tableView.indexPathForSelectedRow?.row
    
    if searchController.isActive && searchController.searchBar.text != "" {
      selectedCard = (filteredCards?[indexPath!])!
    }
    else {
      selectedCard = (availableCards?[indexPath!])!
    }
        
  }
  
  
}


extension CardSelectionTableViewController: UITabBarControllerDelegate{
  
  //To prevent the user from accidentally reseting their arena selection process and making a new deck
  //This function will stop the "Arena" tab from taking the user back to the hero selection screen
  
  func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    
    let destVC = viewController as! UINavigationController
    
    if destVC.viewControllers[0] is HeroSelectionViewController &&
      //self.isViewLoaded &&
      //(self.view.window != nil) &&
      tabBarController.selectedIndex == 0{
      
      return false
    }
    self.searchController.isActive = false
    return true
    
  }
  
}
