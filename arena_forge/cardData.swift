//
//  cardData.swift
//  arena_forge
//
//  Created by Paul Weston on 5/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import Foundation

extension Card{

//    static func addPrototypeCardData(){
//        
//        if(allCards.isEmpty){
//            _ = Card(json:[
//                "cardClass":"PRIEST",
//                "cost":10 as Int8,
//                "dbfId":8,
//                "flavor":"Nominated as \"Spell Most Likely to Make Your Opponent Punch the Wall.\"",
//                "name":"Mind Control",
//                "rarity":"FREE",
//                "text":"Take control of an enemy minion.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PRIEST",
//                "cost":7 as Int8,
//                "dbfId":9,
//                "flavor":"He's been exiled from his home, and all his brothers turned evil, but otherwise he doesn't have a lot to complain about.",
//                "name":"Prophet Velen",
//                "rarity":"LEGENDARY",
//                "text":"Double the damage and healing of your spells and Hero Power.",
//                "type":"MINION",
//                "attack":7 as Int8,
//                "health":7 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":2 as Int8,
//                "dbfId":12,
//                "flavor":"She’s trying to kick the habit, but still takes some mana whenever she has a stressful day.",
//                "name":"Mana Addict",
//                "rarity":"RARE",
//                "text":"Whenever you cast a spell, gain +2 Attack this turn.",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":0 as Int8,
//                "dbfId":21,
//                "flavor":"",
//                "name":"Barrel",
//                "rarity":"COMMON",
//                "text":"Is something in this barrel?",
//                "type":"MINION",
//                "attack":0 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARRIOR",
//                "cost":0 as Int8,
//                "dbfId":22,
//                "flavor":"They're only smiling on the outside.",
//                "name":"Inner Rage",
//                "rarity":"COMMON",
//                "text":"Deal $1 damage to a minion and give it +2 Attack.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARLOCK",
//                "cost":5 as Int8,
//                "dbfId":23,
//                "flavor":"My advice to you is to avoid Doom, if possible.",
//                "name":"Bane of Doom",
//                "rarity":"EPIC",
//                "text":"Deal $2 damage to a character. If that kills it, summon a random Demon.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARRIOR",
//                "cost":4 as Int8,
//                "dbfId":28,
//                "flavor":"The Kor'kron are the elite forces of Garrosh Hellscream. Let's just say you don't want to run into these guys while wearing a blue tabard.",
//                "name":"Kor'kron Elite",
//                "rarity":"FREE",
//                "text":"<b>Charge</b>",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PRIEST",
//                "cost":3 as Int8,
//                "dbfId":30,
//                "flavor":"\"What do you get when you cast Thoughtsteal on an Orc?  Nothing!\" - Tauren joke",
//                "name":"Thoughtsteal",
//                "rarity":"COMMON",
//                "text":"Copy 2 cards in your opponent's deck and add them to your hand.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"HUNTER",
//                "cost":0 as Int8,
//                "dbfId":31,
//                "flavor":"",
//                "name":"Rexxar",
//                "rarity":"FREE",
//                "text":"",
//                "type":"HERO",
//                "attack":0 as Int8,
//                "health":30 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"SHAMAN",
//                "cost":8 as Int8,
//                "dbfId":32,
//                "flavor":"He is the weakest of the four Elemental Lords.  And the other three don't let him forget it.",
//                "name":"Al'Akir the Windlord",
//                "rarity":"LEGENDARY",
//                "text":"<b>Charge, Divine Shield, Taunt, Windfury</b>",
//                "type":"MINION",
//                "attack":3 as Int8,
//                "health":5 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":4 as Int8,
//                "dbfId":34,
//                "flavor":"The first time they tried to guard Silvermoon against the scourge, it didn’t go so well…",
//                "name":"Silvermoon Guardian",
//                "rarity":"COMMON",
//                "text":"<b>Divine Shield</b>",
//                "type":"MINION",
//                "attack":3 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":9 as Int8,
//                "dbfId":36,
//                "flavor":"Yes, he's a demigod. No, he doesn't need to wear a shirt.",
//                "name":"Cenarius",
//                "rarity":"LEGENDARY",
//                "text":"<b>Choose One -</b> Give your other minions +2/+2; or Summon two 2/2 Treants with <b>Taunt</b>.",
//                "type":"MINION",
//                "attack":5 as Int8,
//                "health":8 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":2 as Int8,
//                "dbfId":37,
//                "flavor":"She's quite jealous of the Gallon-Sized Summoner.",
//                "name":"Pint-Sized Summoner",
//                "rarity":"RARE",
//                "text":"The first minion you play each turn costs (1) less.",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":2 as Int8,
//                "dbfId":41,
//                "flavor":"Grunting is what his father did and his father before that.   It's more than just a job.",
//                "name":"Frostwolf Grunt",
//                "rarity":"FREE",
//                "text":"<b>Taunt</b>",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":3 as Int8,
//                "dbfId":45,
//                "flavor":"Tauren Warrior: Champion of Mulgore, Slayer of Quilboar, Rider of Thunderbluff Elevators.",
//                "name":"Tauren Warrior",
//                "rarity":"COMMON",
//                "text":"<b>Taunt</b>\n<b>Enrage:</b> +3 Attack.",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARLOCK",
//                "cost":1 as Int8,
//                "dbfId":48,
//                "flavor":"No relation to \"The Voidsteppers\", the popular Void-based dance troupe.",
//                "name":"Voidwalker",
//                "rarity":"FREE",
//                "text":"<b>Taunt</b>",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"SHAMAN",
//                "cost":2 as Int8,
//                "dbfId":51,
//                "flavor":"Windfury is like Earthfury and Firefury, but more light and airy.",
//                "name":"Windfury",
//                "rarity":"FREE",
//                "text":"Give a minion <b>Windfury</b>.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":52,
//                "flavor":"",
//                "name":"Emboldener 3000",
//                "rarity":"COMMON",
//                "text":"At the end of your turn, give a random minion +1/+1.",
//                "type":"MINION",
//                "attack":0 as Int8,
//                "health":4 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":59,
//                "flavor":"",
//                "name":"Mechanical Dragonling",
//                "rarity":"COMMON",
//                "text":"",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":60,
//                "flavor":"",
//                "name":"Summon a Panther",
//                "rarity":"",
//                "text":"Summon a 3/2 Panther.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":5 as Int8,
//                "dbfId":61,
//                "flavor":"She'll craft you a sword, but you'll need to bring her 5 Steel Ingots, 3 Motes of Earth, and the scalp of her last customer.",
//                "name":"Spiteful Smith",
//                "rarity":"COMMON",
//                "text":"<b>Enrage:</b> Your weapon has +2 Attack.",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":6 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":63,
//                "flavor":"",
//                "name":"Cat Form",
//                "rarity":"",
//                "text":"<b>Charge</b>",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":4 as Int8,
//                "dbfId":64,
//                "flavor":"When a bear rears back and extends his arms, he's about to Swipe!  ... or hug.",
//                "name":"Swipe",
//                "rarity":"FREE",
//                "text":"Deal $4 damage to an enemy and $1 damage to all other enemies.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":3 as Int8,
//                "dbfId":67,
//                "flavor":"He likes to act like he's in charge, but the silverback matriarch actually runs things.",
//                "name":"Silverback Patriarch",
//                "rarity":"FREE",
//                "text":"<b>Taunt</b>",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":4 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":5 as Int8,
//                "dbfId":68,
//                "flavor":"The wonderful thing about tigers is tigers are wonderful things!",
//                "name":"Stranglethorn Tiger",
//                "rarity":"COMMON",
//                "text":"<b>Stealth</b>",
//                "type":"MINION",
//                "attack":5 as Int8,
//                "health":5 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":5 as Int8,
//                "dbfId":69,
//                "flavor":"It's good to be a knight.   Less so to be one's squire.",
//                "name":"Silver Hand Knight",
//                "rarity":"COMMON",
//                "text":"<b>Battlecry:</b> Summon a 2/2 Squire.",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":4 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PALADIN",
//                "cost":1 as Int8,
//                "dbfId":70,
//                "flavor":"\"As in, you MIGHT want to get out of my way.\" - Toad Mackle, recently buffed.",
//                "name":"Blessing of Might",
//                "rarity":"FREE",
//                "text":"Give a minion +3 Attack.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARRIOR",
//                "cost":5 as Int8,
//                "dbfId":75,
//                "flavor":"Do you know the first rule of Brawl Club?",
//                "name":"Brawl",
//                "rarity":"EPIC",
//                "text":"Destroy all minions except one. <i>(chosen randomly)</i>",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":76,
//                "flavor":"",
//                "name":"Imp",
//                "rarity":"",
//                "text":"",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"MAGE",
//                "cost":4 as Int8,
//                "dbfId":77,
//                "flavor":"There was going to be a pun in this flavor text, but it just came out baa-d.",
//                "name":"Polymorph",
//                "rarity":"FREE",
//                "text":"Transform a minion\ninto a 1/1 Sheep.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARRIOR",
//                "cost":0 as Int8,
//                "dbfId":78,
//                "flavor":"",
//                "name":"Heroic Strike",
//                "rarity":"",
//                "text":"+4 Attack this turn.",
//                "type":"ENCHANTMENT",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":5 as Int8,
//                "dbfId":86,
//                "flavor":"Is the sky falling?  Yes.  Yes it is.",
//                "name":"Starfall",
//                "rarity":"RARE",
//                "text":"<b>Choose One -</b>\nDeal $5 damage to a minion; or $2 damage to all enemy minions.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":4 as Int8,
//                "dbfId":90,
//                "flavor":"He always dreamed of coming down from the mountains and opening a noodle shop, but he never got the nerve.",
//                "name":"Chillwind Yeti",
//                "rarity":"FREE",
//                "text":"",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":5 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":5 as Int8,
//                "dbfId":95,
//                "flavor":"Druids take nourishment from many things: the power of nature, the songbird's chirp, a chocolate cake.",
//                "name":"Nourish",
//                "rarity":"RARE",
//                "text":"<b>Choose One -</b> Gain 2 Mana Crystals; or Draw 3 cards.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":99,
//                "flavor":"",
//                "name":"Bear Form",
//                "rarity":"",
//                "text":"+2 Health and <b>Taunt</b>.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"HUNTER",
//                "cost":3 as Int8,
//                "dbfId":100,
//                "flavor":"",
//                "name":"Huffer",
//                "rarity":"COMMON",
//                "text":"<b>Charge</b>",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PALADIN",
//                "cost":1 as Int8,
//                "dbfId":102,
//                "flavor":"",
//                "name":"Defender",
//                "rarity":"COMMON",
//                "text":"",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"MAGE",
//                "cost":3 as Int8,
//                "dbfId":113,
//                "flavor":"What's the difference between a mage playing with Counterspell and a mage who isn't?  The mage who isn't is getting Pyroblasted in the face.",
//                "name":"Counterspell",
//                "rarity":"RARE",
//                "text":"<b>Secret:</b> When your opponent casts a spell, <b>Counter</b> it.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":132,
//                "flavor":"Voodoo is an oft-misunderstood art. But it <i>is</i> art.",
//                "name":"Voodoo Doctor",
//                "rarity":"FREE",
//                "text":"<b>Battlecry:</b> Restore 2 Health.",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":7 as Int8,
//                "dbfId":134,
//                "flavor":"Just mail him a package with a name and 10,000 gold.  He'll take care of the rest.",
//                "name":"Ravenholdt Assassin",
//                "rarity":"RARE",
//                "text":"<b>Stealth</b>",
//                "type":"MINION",
//                "attack":7 as Int8,
//                "health":5 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":2 as Int8,
//                "dbfId":138,
//                "flavor":"He's almost been right so many times. He was <i>sure</i> it was coming during the Cataclysm.",
//                "name":"Doomsayer",
//                "rarity":"EPIC",
//                "text":"At the start of your turn, destroy ALL minions.",
//                "type":"MINION",
//                "attack":0 as Int8,
//                "health":7 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PALADIN",
//                "cost":1 as Int8,
//                "dbfId":140,
//                "flavor":"I am not sure how you get demptioned the first time.  It’s a mystery!",
//                "name":"Redemption",
//                "rarity":"COMMON",
//                "text":"<b>Secret:</b> When a friendly minion dies, return it to life with 1 Health.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"HUNTER",
//                "cost":1 as Int8,
//                "dbfId":141,
//                "flavor":"Never play 'Hide and Go Seek' with a Hunter.",
//                "name":"Hunter's Mark",
//                "rarity":"FREE",
//                "text":"Change a minion's Health to 1.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PRIEST",
//                "cost":4 as Int8,
//                "dbfId":145,
//                "flavor":"Sometimes it feels like this is all a game.",
//                "name":"Mindgames",
//                "rarity":"EPIC",
//                "text":"Put a copy of\na random minion from\nyour opponent's deck into the battlefield.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":146,
//                "flavor":"",
//                "name":"Poultryizer",
//                "rarity":"COMMON",
//                "text":"At the start of your turn, transform a random minion into a 1/1 Chicken.",
//                "type":"MINION",
//                "attack":0 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARLOCK",
//                "cost":4 as Int8,
//                "dbfId":147,
//                "flavor":"Start with a powerful minion and stir in Shadowflame and you have a good time!",
//                "name":"Shadowflame",
//                "rarity":"RARE",
//                "text":"Destroy a friendly minion and deal its Attack damage to all enemy minions.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"SHAMAN",
//                "cost":0 as Int8,
//                "dbfId":149,
//                "flavor":"I personally prefer some non-ancestral right-the-heck-now healing, but maybe that is just me.",
//                "name":"Ancestral Healing",
//                "rarity":"FREE",
//                "text":"Restore a minion\nto full Health and\ngive it <b>Taunt</b>.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":3 as Int8,
//                "dbfId":151,
//                "flavor":"Druids call it the \"Mark of Nature.\"  Everyone else calls it \"needing a bath.\"",
//                "name":"Mark of Nature",
//                "rarity":"COMMON",
//                "text":"<b>Choose One -</b> Give a minion +4 Attack; or +4 Health and <b>Taunt</b>.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":153,
//                "flavor":"",
//                "name":"Rooted",
//                "rarity":"",
//                "text":"+5 Health and <b>Taunt</b>.",
//                "type":"ENCHANTMENT",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":6 as Int8,
//                "dbfId":157,
//                "flavor":"He used to be a 2100+ rated arena player, but that was years ago and nobody can get him to shut up about it.",
//                "name":"Lord of the Arena",
//                "rarity":"FREE",
//                "text":"<b>Taunt</b>",
//                "type":"MINION",
//                "attack":6 as Int8,
//                "health":5 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":158,
//                "flavor":"She promises not to tell anyone about that thing you did last night with that one person.",
//                "name":"Secretkeeper",
//                "rarity":"RARE",
//                "text":"Whenever a <b>Secret</b> is played, gain +1/+1.",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARLOCK",
//                "cost":0 as Int8,
//                "dbfId":163,
//                "flavor":"This is the reason that Demons never really become friends with Warlocks.",
//                "name":"Sacrificial Pact",
//                "rarity":"FREE",
//                "text":"Destroy a Demon. Restore #5 Health to your hero.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PALADIN",
//                "cost":0 as Int8,
//                "dbfId":169,
//                "flavor":"",
//                "name":"Repentance",
//                "rarity":"",
//                "text":"Health reduced to 1.",
//                "type":"ENCHANTMENT",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"MAGE",
//                "cost":1 as Int8,
//                "dbfId":172,
//                "flavor":"The trick is not to break the lance.  Otherwise, you have \"Ice Pieces.\"  Ice Pieces aren't as effective.",
//                "name":"Ice Lance",
//                "rarity":"COMMON",
//                "text":"<b>Freeze</b> a character. If it was already <b>Frozen</b>, deal $4 damage instead.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":3 as Int8,
//                "dbfId":175,
//                "flavor":"You don't see a lot of Dalaran warriors.",
//                "name":"Dalaran Mage",
//                "rarity":"FREE",
//                "text":"<b>Spell Damage +1</b>",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":4 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"SHAMAN",
//                "cost":4 as Int8,
//                "dbfId":178,
//                "flavor":"Is there anything worse than a Windspeaker with halitosis?",
//                "name":"Windspeaker",
//                "rarity":"FREE",
//                "text":"<b>Battlecry:</b> Give a friendly minion <b>Windfury</b>.",
//                "type":"MINION",
//                "attack":3 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":0 as Int8,
//                "dbfId":179,
//                "flavor":"If you hit an Eredar Lord with enough Wisps, it will explode.   But why?",
//                "name":"Wisp",
//                "rarity":"COMMON",
//                "text":"",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":0 as Int8,
//                "dbfId":180,
//                "flavor":"It's funny how often yelling \"Look over there!\" gets your opponent to turn around.",
//                "name":"Backstab",
//                "rarity":"FREE",
//                "text":"Deal $2 damage to an undamaged minion.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":182,
//                "flavor":"",
//                "name":"Uproot",
//                "rarity":"",
//                "text":"+5 Attack.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":4 as Int8,
//                "dbfId":186,
//                "flavor":"Most pandaren say his brew tastes like yak.  But apparently that's a compliment.",
//                "name":"Ancient Brewmaster",
//                "rarity":"COMMON",
//                "text":"<b>Battlecry:</b> Return a friendly minion from the battlefield to your hand.",
//                "type":"MINION",
//                "attack":5 as Int8,
//                "health":4 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"SHAMAN",
//                "cost":6 as Int8,
//                "dbfId":189,
//                "flavor":"He can never take a bath. Ewww.",
//                "name":"Fire Elemental",
//                "rarity":"FREE",
//                "text":"<b>Battlecry:</b> Deal 3 damage.",
//                "type":"MINION",
//                "attack":6 as Int8,
//                "health":5 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":191,
//                "flavor":"Mrrraggglhlhghghlgh, mrgaaag blarrghlgaahahl mrgggg glhalhah a bghhll graggmgmg Garrosh mglhlhlh mrghlhlhl!!",
//                "name":"Murloc Raider",
//                "rarity":"FREE",
//                "text":"",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"MAGE",
//                "cost":3 as Int8,
//                "dbfId":192,
//                "flavor":"Ice is nice, and will suffice!",
//                "name":"Ice Block",
//                "rarity":"EPIC",
//                "text":"<b>Secret:</b> When your hero takes fatal damage, prevent it and become <b>Immune</b> this turn.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"MAGE",
//                "cost":3 as Int8,
//                "dbfId":195,
//                "flavor":"\"You go first.\" - Krush'gor the Behemoth, to his pet boar.",
//                "name":"Mirror Entity",
//                "rarity":"COMMON",
//                "text":"<b>Secret:</b> After your opponent plays a minion, summon a copy of it.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":6 as Int8,
//                "dbfId":196,
//                "flavor":"",
//                "name":"Vanish",
//                "rarity":"FREE",
//                "text":"Return all minions to their owner's hand.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":2 as Int8,
//                "dbfId":201,
//                "flavor":"He stole the deed to town years ago, so technically the town <i>is</i> his. He just calls people Scrub to be mean.",
//                "name":"Defias Ringleader",
//                "rarity":"COMMON",
//                "text":"<b>Combo:</b> Summon a 2/1 Defias Bandit.",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"HUNTER",
//                "cost":1 as Int8,
//                "dbfId":204,
//                "flavor":"",
//                "name":"Snake",
//                "rarity":"COMMON",
//                "text":"",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":8 as Int8,
//                "dbfId":205,
//                "flavor":"I <i>dare</i> you to attack Darnassus.",
//                "name":"Ironbark Protector",
//                "rarity":"FREE",
//                "text":"<b>Taunt</b>",
//                "type":"MINION",
//                "attack":8 as Int8,
//                "health":8 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":0 as Int8,
//                "dbfId":208,
//                "flavor":"",
//                "name":"Nightmare",
//                "rarity":"",
//                "text":"This minion has +5/+5, but will be destroyed soon.",
//                "type":"ENCHANTMENT",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":209,
//                "flavor":"",
//                "name":"Ancient Secrets",
//                "rarity":"",
//                "text":"Restore 5 Health.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":10 as Int8,
//                "dbfId":211,
//                "flavor":"See?  Giant.",
//                "name":"Sea Giant",
//                "rarity":"EPIC",
//                "text":"Costs (1) less for each other minion on the battlefield.",
//                "type":"MINION",
//                "attack":8 as Int8,
//                "health":8 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":2 as Int8,
//                "dbfId":213,
//                "flavor":"Not to be confused with Jim of the Wild.",
//                "name":"Mark of the Wild",
//                "rarity":"FREE",
//                "text":"Give a minion <b>Taunt</b> and +2/+2.<i>\n(+2 Attack/+2 Health)</i>",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":2 as Int8,
//                "dbfId":216,
//                "flavor":"\"Kill 30 raptors.\" - Hemet Nesingwary",
//                "name":"Bloodfen Raptor",
//                "rarity":"FREE",
//                "text":"",
//                "type":"MINION",
//                "attack":3 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DREAM",
//                "cost":0 as Int8,
//                "dbfId":217,
//                "flavor":"",
//                "name":"Nightmare",
//                "rarity":"",
//                "text":"Give a minion +5/+5. At the start of your next turn, destroy it.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PRIEST",
//                "cost":4 as Int8,
//                "dbfId":220,
//                "flavor":"You can rationalize it all you want, it's still a mean thing to do.",
//                "name":"Shadow Madness",
//                "rarity":"RARE",
//                "text":"Gain control of an enemy minion with 3 or less Attack until end of turn.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"HUNTER",
//                "cost":3 as Int8,
//                "dbfId":226,
//                "flavor":"",
//                "name":"Leokk",
//                "rarity":"COMMON",
//                "text":"Your other minions have +1 Attack.",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":4 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":227,
//                "flavor":"",
//                "name":"Homing Chicken",
//                "rarity":"COMMON",
//                "text":"At the start of your turn, destroy this minion and draw 3 cards.",
//                "type":"MINION",
//                "attack":0 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"HUNTER",
//                "cost":2 as Int8,
//                "dbfId":229,
//                "flavor":"",
//                "name":"Steady Shot",
//                "rarity":"FREE",
//                "text":"<b>Hero Power</b>\nDeal $2 damage to the enemy hero.",
//                "type":"HERO_POWER",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PALADIN",
//                "cost":1 as Int8,
//                "dbfId":232,
//                "flavor":"Repentance often comes in the moment before obliteration. Curious.",
//                "name":"Repentance",
//                "rarity":"COMMON",
//                "text":"<b>Secret:</b> After your opponent plays a minion, reduce its Health to 1.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":1 as Int8,
//                "dbfId":233,
//                "flavor":"Another one bites the dust.",
//                "name":"Naturalize",
//                "rarity":"COMMON",
//                "text":"Destroy a minion.\nYour opponent draws 2 cards.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PRIEST",
//                "cost":4 as Int8,
//                "dbfId":237,
//                "flavor":"The Auchenai know the end is coming, but they're not sure when.",
//                "name":"Auchenai Soulpriest",
//                "rarity":"RARE",
//                "text":"Your cards and powers that restore Health now deal damage instead.",
//                "type":"MINION",
//                "attack":3 as Int8,
//                "health":5 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"SHAMAN",
//                "cost":3 as Int8,
//                "dbfId":238,
//                "flavor":"Spirit wolves are like regular wolves with pom-poms.",
//                "name":"Feral Spirit",
//                "rarity":"RARE",
//                "text":"Summon two 2/3 Spirit Wolves with <b>Taunt</b>. <b>Overload:</b> (2)",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"SHAMAN",
//                "cost":2 as Int8,
//                "dbfId":239,
//                "flavor":"This would be real handy if your enemy is made of rock.",
//                "name":"Rockbiter Weapon",
//                "rarity":"FREE",
//                "text":"Give a friendly character +3 Attack this turn.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":242,
//                "flavor":"ADD ME TO YOUR DECK, MAGGOT!",
//                "name":"Abusive Sergeant",
//                "rarity":"COMMON",
//                "text":"<b>Battlecry:</b> Give a minion +2 Attack this turn.",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PALADIN",
//                "cost":4 as Int8,
//                "dbfId":250,
//                "flavor":"A good paladin has many tools.  Hammer of Wrath, Pliers of Vengeance, Hacksaw of Justice, etc.",
//                "name":"Hammer of Wrath",
//                "rarity":"FREE",
//                "text":"Deal $3 damage.\nDraw a card.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":2 as Int8,
//                "dbfId":251,
//                "flavor":"Always roll need.",
//                "name":"Loot Hoarder",
//                "rarity":"COMMON",
//                "text":"<b>Deathrattle:</b> Draw a card.",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":253,
//                "flavor":"",
//                "name":"Wrath",
//                "rarity":"",
//                "text":"Deal $3 damage to a minion.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":254,
//                "flavor":"Some druids still have flashbacks from strangers yelling \"Innervate me!!\" at them.",
//                "name":"Innervate",
//                "rarity":"FREE",
//                "text":"Gain 1 Mana Crystal this turn only.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":3 as Int8,
//                "dbfId":257,
//                "flavor":"Someone did mess with Tuskerr once.  ONCE.",
//                "name":"Razorfen Hunter",
//                "rarity":"FREE",
//                "text":"<b>Battlecry:</b> Summon a 1/1 Boar.",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":0 as Int8,
//                "dbfId":262,
//                "flavor":"",
//                "name":"Chicken",
//                "rarity":"",
//                "text":"<i>Hey Chicken!</i>",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":1 as Int8,
//                "dbfId":268,
//                "flavor":"\"I'm cold blooded, check it and see!\"",
//                "name":"Cold Blood",
//                "rarity":"COMMON",
//                "text":"Give a minion +2 Attack. <b>Combo:</b> +4 Attack instead.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PRIEST",
//                "cost":6 as Int8,
//                "dbfId":272,
//                "flavor":"You never know who may be secretly working for the Cabal....",
//                "name":"Cabal Shadow Priest",
//                "rarity":"EPIC",
//                "text":"<b>Battlecry:</b> Take control of an enemy minion that has 2 or less Attack.",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":5 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":274,
//                "flavor":"",
//                "name":"Malfurion Stormrage",
//                "rarity":"FREE",
//                "text":"",
//                "type":"HERO",
//                "attack":0 as Int8,
//                "health":30 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PRIEST",
//                "cost":1 as Int8,
//                "dbfId":279,
//                "flavor":"It doesn't matter how pious you are.  Everyone needs a good smiting now and again.",
//                "name":"Holy Smite",
//                "rarity":"FREE",
//                "text":"Deal $2 damage.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":6 as Int8,
//                "dbfId":281,
//                "flavor":"The Argent Dawn stands vigilant against the Scourge, as well as people who cut in line at coffee shops.",
//                "name":"Argent Commander",
//                "rarity":"RARE",
//                "text":"<b>Charge</b>\n<b>Divine Shield</b>",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":2 as Int8,
//                "dbfId":282,
//                "flavor":"Everyone has a price. Gnomes, for example, can be persuaded by stuffed animals and small amounts of chocolate.",
//                "name":"Betrayal",
//                "rarity":"COMMON",
//                "text":"Force an enemy minion to deal its damage to the minions next to it.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":2 as Int8,
//                "dbfId":284,
//                "flavor":"\"Half of this class will not graduate… since they'll have been turned to chickens.\" - Tinkmaster Overspark, teaching Gizmos 101.",
//                "name":"Novice Engineer",
//                "rarity":"FREE",
//                "text":"<b>Battlecry:</b> Draw a card.",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARRIOR",
//                "cost":2 as Int8,
//                "dbfId":285,
//                "flavor":"\"I'm going to need you to come in on Sunday.\" - Cruel Taskmaster",
//                "name":"Cruel Taskmaster",
//                "rarity":"COMMON",
//                "text":"<b>Battlecry:</b> Deal 1 damage to a minion and give it +2 Attack.",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"MAGE",
//                "cost":3 as Int8,
//                "dbfId":286,
//                "flavor":"Rumor has it that Deathwing brought about the Cataclysm after losing a game to this card.  We may never know the truth.",
//                "name":"Vaporize",
//                "rarity":"RARE",
//                "text":"<b>Secret:</b> When a minion attacks your hero, destroy it.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":6 as Int8,
//                "dbfId":287,
//                "flavor":"He just wants people to see his vacation photos.",
//                "name":"Kidnapper",
//                "rarity":"EPIC",
//                "text":"<b>Combo:</b> Return a minion to its owner's hand.",
//                "type":"MINION",
//                "attack":5 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":3 as Int8,
//                "dbfId":289,
//                "flavor":"Orcish raiders ride wolves because they are well adapted to harsh environments, and because they are soft and cuddly.",
//                "name":"Wolfrider",
//                "rarity":"FREE",
//                "text":"<b>Charge</b>",
//                "type":"MINION",
//                "attack":3 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":3 as Int8,
//                "dbfId":290,
//                "flavor":"Their wings are silent but their screech is... whatever the opposite of silent is.",
//                "name":"Ironbeak Owl",
//                "rarity":"COMMON",
//                "text":"<b>Battlecry:</b> <b>Silence</b> a minion.",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PALADIN",
//                "cost":2 as Int8,
//                "dbfId":291,
//                "flavor":"If you are often bathed in Holy Light, you should consider wearing sunscreen.",
//                "name":"Holy Light",
//                "rarity":"FREE",
//                "text":"Restore #6 Health.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"HUNTER",
//                "cost":4 as Int8,
//                "dbfId":292,
//                "flavor":"You see, it's all about <i>throughput</i>.",
//                "name":"Multi-Shot",
//                "rarity":"FREE",
//                "text":"Deal $3 damage to two random enemy minions.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"HUNTER",
//                "cost":3 as Int8,
//                "dbfId":296,
//                "flavor":"\"Kill!\", he commanded.",
//                "name":"Kill Command",
//                "rarity":"FREE",
//                "text":"Deal $3 damage. If you control a Beast, deal\n$5 damage instead.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":298,
//                "flavor":"",
//                "name":"Boar",
//                "rarity":"COMMON",
//                "text":"",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"SHAMAN",
//                "cost":1 as Int8,
//                "dbfId":299,
//                "flavor":"If you combine it with Spooned Lightning and Knived Lightning, you have the full dining set.",
//                "name":"Forked Lightning",
//                "rarity":"COMMON",
//                "text":"Deal $2 damage to 2 random enemy minions. <b>Overload:</b> (2)",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARLOCK",
//                "cost":2 as Int8,
//                "dbfId":300,
//                "flavor":"",
//                "name":"Life Tap",
//                "rarity":"FREE",
//                "text":"<b>Hero Power</b>\nDraw a card and take $2 damage.",
//                "type":"HERO_POWER",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DREAM",
//                "cost":2 as Int8,
//                "dbfId":301,
//                "flavor":"",
//                "name":"Ysera Awakens",
//                "rarity":"",
//                "text":"Deal $5 damage to all characters except Ysera.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARRIOR",
//                "cost":5 as Int8,
//                "dbfId":304,
//                "flavor":"No… actually you should fear the Reaper.",
//                "name":"Arcanite Reaper",
//                "rarity":"FREE",
//                "text":"",
//                "type":"WEAPON",
//                "attack":5 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":3 as Int8,
//                "dbfId":306,
//                "flavor":"He led the Stonemasons in the reconstruction of Stormwind, and when the nobles refused to pay, he founded the Defias Brotherhood to, well, <i>deconstruct</i> Stormwind.",
//                "name":"Edwin VanCleef",
//                "rarity":"LEGENDARY",
//                "text":"<b>Combo:</b> Gain +2/+2 for each other card you've played this turn.",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":4 as Int8,
//                "dbfId":308,
//                "flavor":"She's never quite sure what she's making, she just knows it's AWESOME!",
//                "name":"Gnomish Inventor",
//                "rarity":"FREE",
//                "text":"<b>Battlecry:</b> Draw a card.",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":4 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"HUNTER",
//                "cost":7 as Int8,
//                "dbfId":311,
//                "flavor":"The longbow allows shots to be fired from farther away and is useful for firing on particularly odorous targets.",
//                "name":"Gladiator's Longbow",
//                "rarity":"EPIC",
//                "text":"Your hero is <b>Immune</b> while attacking.",
//                "type":"WEAPON",
//                "attack":5 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":313,
//                "flavor":"",
//                "name":"Ancient Teachings",
//                "rarity":"",
//                "text":"Draw a card.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"MAGE",
//                "cost":4 as Int8,
//                "dbfId":315,
//                "flavor":"This spell is useful for burning things.  If you're looking for spells that toast things, or just warm them a little, you're in the wrong place.",
//                "name":"Fireball",
//                "rarity":"FREE",
//                "text":"Deal $6 damage.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":317,
//                "flavor":"",
//                "name":"Bananas",
//                "rarity":"COMMON",
//                "text":"Give a friendly minion +1/+1. <i>(+1 Attack/+1 Health)</i>",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":4 as Int8,
//                "dbfId":318,
//                "flavor":"",
//                "name":"Baine Bloodhoof",
//                "rarity":"",
//                "text":"",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":5 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":321,
//                "flavor":"",
//                "name":"Dispel",
//                "rarity":"",
//                "text":"<b>Silence</b> a minion.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":322,
//                "flavor":"",
//                "name":"Mark of Nature",
//                "rarity":"",
//                "text":"This minion has +4 Attack.",
//                "type":"ENCHANTMENT",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":325,
//                "flavor":"",
//                "name":"Nourish",
//                "rarity":"",
//                "text":"Draw 3 cards.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":329,
//                "flavor":"",
//                "name":"Repair Bot",
//                "rarity":"COMMON",
//                "text":"At the end of your turn, restore 6 Health to a damaged character.",
//                "type":"MINION",
//                "attack":0 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":331,
//                "flavor":"",
//                "name":"Demigod's Favor",
//                "rarity":"",
//                "text":"",
//                "type":"ENCHANTMENT",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":5 as Int8,
//                "dbfId":332,
//                "flavor":"",
//                "name":"Devilsaur",
//                "rarity":"COMMON",
//                "text":"",
//                "type":"MINION",
//                "attack":5 as Int8,
//                "health":5 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":7 as Int8,
//                "dbfId":336,
//                "flavor":"Baron Geddon was Ragnaros's foremost lieutenant, until he got FIRED.",
//                "name":"Baron Geddon",
//                "rarity":"LEGENDARY",
//                "text":"At the end of your turn, deal 2 damage to ALL other characters.",
//                "type":"MINION",
//                "attack":7 as Int8,
//                "health":5 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARRIOR",
//                "cost":8 as Int8,
//                "dbfId":338,
//                "flavor":"Grommash drank the tainted blood of Mannoroth, dooming the orcs to green skin and red eyes!  Maybe not his best decision.",
//                "name":"Grommash Hellscream",
//                "rarity":"LEGENDARY",
//                "text":"<b>Charge</b>\n<b>Enrage:</b> +6 Attack",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":9 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":3 as Int8,
//                "dbfId":339,
//                "flavor":"\"Ready! Aim! Drink!\"",
//                "name":"Ironforge Rifleman",
//                "rarity":"FREE",
//                "text":"<b>Battlecry:</b> Deal 1 damage.",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DREAM",
//                "cost":3 as Int8,
//                "dbfId":340,
//                "flavor":"",
//                "name":"Laughing Sister",
//                "rarity":"",
//                "text":"Can't be targeted by spells or Hero Powers.",
//                "type":"MINION",
//                "attack":3 as Int8,
//                "health":5 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARRIOR",
//                "cost":1 as Int8,
//                "dbfId":344,
//                "flavor":"\"Guys! Guys! Slow down!\" - some kind of non-warrior minion",
//                "name":"Charge",
//                "rarity":"FREE",
//                "text":"Give a friendly minion <b>Charge</b>. It can't attack heroes this turn.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":5 as Int8,
//                "dbfId":345,
//                "flavor":"If you don't want to be assassinated, move to the Barrens and change your name. Good luck!",
//                "name":"Assassinate",
//                "rarity":"FREE",
//                "text":"Destroy an enemy minion.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":4 as Int8,
//                "dbfId":348,
//                "flavor":"Guardians of Dark Iron Ore.  Perhaps the most annoying ore, given where you have to forge it.",
//                "name":"Dark Iron Dwarf",
//                "rarity":"COMMON",
//                "text":"<b>Battlecry:</b> Give a minion +2 Attack this turn.",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":4 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":2 as Int8,
//                "dbfId":351,
//                "flavor":"He's currently trying to craft a \"flail-axe\", but all the other swordsmiths say it can't be done.",
//                "name":"Master Swordsmith",
//                "rarity":"RARE",
//                "text":"At the end of your turn, give another random friendly minion +1 Attack.",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"SHAMAN",
//                "cost":5 as Int8,
//                "dbfId":352,
//                "flavor":"Orgrim Doomhammer gave this legendary weapon to Thrall.  His name is a total coincidence.",
//                "name":"Doomhammer",
//                "rarity":"EPIC",
//                "text":"<b>Windfury, Overload:</b> (2)",
//                "type":"WEAPON",
//                "attack":2 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":2 as Int8,
//                "dbfId":358,
//                "flavor":"",
//                "name":"Treant",
//                "rarity":"COMMON",
//                "text":"",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":9 as Int8,
//                "dbfId":363,
//                "flavor":"Onyxia long manipulated the Stormwind Court by disguising herself as Lady Katrana Prestor.   You would have thought that the giant wings and scales would have been a giveaway.",
//                "name":"Onyxia",
//                "rarity":"LEGENDARY",
//                "text":"<b>Battlecry:</b> Summon 1/1 Whelps until your side of the battlefield is full.",
//                "type":"MINION",
//                "attack":8 as Int8,
//                "health":8 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":364,
//                "flavor":"",
//                "name":"Shan'do's Lesson",
//                "rarity":"",
//                "text":"Summon two 2/2 Treants with <b>Taunt</b>.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":0 as Int8,
//                "dbfId":365,
//                "flavor":"Rogue dance troops will sometimes Shadowstep away at the end of a performance.  Crowds love it.",
//                "name":"Shadowstep",
//                "rarity":"COMMON",
//                "text":"Return a friendly minion to your hand. It costs (2) less.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"MAGE",
//                "cost":3 as Int8,
//                "dbfId":366,
//                "flavor":"While it's fun to intercept enemy lightning bolts, a spellbender much prefers to intercept opposing Marks of the Wild.  It just feels meaner.  And blood elves... well, they're a little mean.",
//                "name":"Spellbender",
//                "rarity":"EPIC",
//                "text":"<b>Secret:</b> When an enemy casts a spell on a minion, summon a 1/3 as the new target.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":8 as Int8,
//                "dbfId":374,
//                "flavor":"Ragnaros was summoned by the Dark Iron dwarves, who were eventually enslaved by the Firelord.  Summoning Ragnaros often doesn’t work out the way you want it to.",
//                "name":"Ragnaros the Firelord",
//                "rarity":"LEGENDARY",
//                "text":"Can't attack. At the end of your turn, deal 8 damage to a random enemy.",
//                "type":"MINION",
//                "attack":8 as Int8,
//                "health":8 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PRIEST",
//                "cost":1 as Int8,
//                "dbfId":376,
//                "flavor":"Good idea: Buffing your minions.  Bad idea: Starting a conversation in the Barrens.",
//                "name":"Inner Fire",
//                "rarity":"COMMON",
//                "text":"Change a minion's Attack to be equal to its Health.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":378,
//                "flavor":"",
//                "name":"Violet Apprentice",
//                "rarity":"",
//                "text":"",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":4 as Int8,
//                "dbfId":381,
//                "flavor":"\"Reforestation\" is suddenly a terrifying word.",
//                "name":"Soul of the Forest",
//                "rarity":"COMMON",
//                "text":"Give your minions \"<b>Deathrattle:</b> Summon a 2/2 Treant.\"",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PALADIN",
//                "cost":1 as Int8,
//                "dbfId":383,
//                "flavor":"Prince Malchezaar was a collector of rare weapons. He'd animate them and have them dance for him.",
//                "name":"Light's Justice",
//                "rarity":"FREE",
//                "text":"",
//                "type":"WEAPON",
//                "attack":1 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":389,
//                "flavor":"Don't bother asking her out on a date.  She'll shoot you down.",
//                "name":"Elven Archer",
//                "rarity":"FREE",
//                "text":"<b>Battlecry:</b> Deal 1 damage.",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":3 as Int8,
//                "dbfId":391,
//                "flavor":"Perdition's Blade is Ragnaros's back-up weapon while Sulfuras is in the shop.",
//                "name":"Perdition's Blade",
//                "rarity":"RARE",
//                "text":"<b>Battlecry:</b> Deal 1 damage. <b>Combo:</b> Deal 2 instead.",
//                "type":"WEAPON",
//                "attack":2 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"HUNTER",
//                "cost":5 as Int8,
//                "dbfId":394,
//                "flavor":"Pull the pin, count to 5, then shoot.  Then duck.",
//                "name":"Explosive Shot",
//                "rarity":"RARE",
//                "text":"Deal $5 damage to a minion and $2 damage to adjacent ones.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"MAGE",
//                "cost":4 as Int8,
//                "dbfId":395,
//                "flavor":"Don't summon a water elemental at a party.  It'll dampen the mood.",
//                "name":"Water Elemental",
//                "rarity":"FREE",
//                "text":"<b>Freeze</b> any character damaged by this minion.",
//                "type":"MINION",
//                "attack":3 as Int8,
//                "health":6 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":3 as Int8,
//                "dbfId":397,
//                "flavor":"'Flesheating' is an unfair name.  It's just that there's not really much else for him to eat.",
//                "name":"Flesheating Ghoul",
//                "rarity":"COMMON",
//                "text":"Whenever a minion dies, gain +1 Attack.",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARRIOR",
//                "cost":2 as Int8,
//                "dbfId":400,
//                "flavor":"\"You won't like me when I'm angry.\"",
//                "name":"Battle Rage",
//                "rarity":"COMMON",
//                "text":"Draw a card for each damaged friendly character.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARRIOR",
//                "cost":3 as Int8,
//                "dbfId":401,
//                "flavor":"During times of tranquility and harmony, this weapon was called by its less popular name, Chilly Peace Axe.",
//                "name":"Fiery War Axe",
//                "rarity":"FREE",
//                "text":"",
//                "type":"WEAPON",
//                "attack":3 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"SHAMAN",
//                "cost":2 as Int8,
//                "dbfId":404,
//                "flavor":"It was just a flesh wound.",
//                "name":"Ancestral Spirit",
//                "rarity":"RARE",
//                "text":"Give a minion \"<b>Deathrattle:</b> Resummon this minion.\"",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"MAGE",
//                "cost":1 as Int8,
//                "dbfId":405,
//                "flavor":"These wyrms feed on arcane energies, and while they are generally considered a nuisance rather than a real threat, you really shouldn't leave them alone with a bucket of mana.",
//                "name":"Mana Wyrm",
//                "rarity":"COMMON",
//                "text":"Whenever you cast a spell, gain +1 Attack.",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":9 as Int8,
//                "dbfId":411,
//                "flavor":"Time to write some flavor text.",
//                "name":"Nozdormu",
//                "rarity":"LEGENDARY",
//                "text":"Players only have 15 seconds to take their turns.",
//                "type":"MINION",
//                "attack":8 as Int8,
//                "health":8 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":5 as Int8,
//                "dbfId":413,
//                "flavor":"The Stormpike Commandos are demolition experts.  They also bake a mean cupcake.",
//                "name":"Stormpike Commando",
//                "rarity":"FREE",
//                "text":"<b>Battlecry:</b> Deal 2 damage.",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":2 as Int8,
//                "dbfId":415,
//                "flavor":"His youthful enthusiasm doesn’t always equal excellence in his brews.   Don’t drink the Mogu Stout!",
//                "name":"Youthful Brewmaster",
//                "rarity":"COMMON",
//                "text":"<b>Battlecry:</b> Return a friendly minion from the battlefield to your hand.",
//                "type":"MINION",
//                "attack":3 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":6 as Int8,
//                "dbfId":420,
//                "flavor":"Cairne was killed by Garrosh, so... don't put this guy in a Warrior deck.  It's pretty insensitive.",
//                "name":"Cairne Bloodhoof",
//                "rarity":"LEGENDARY",
//                "text":"<b>Deathrattle:</b> Summon a 4/5 Baine Bloodhoof.",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":5 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":5 as Int8,
//                "dbfId":421,
//                "flavor":"Guaranteed to have been owned by a real assassin.   Certificate of authenticity included.",
//                "name":"Assassin's Blade",
//                "rarity":"FREE",
//                "text":"",
//                "type":"WEAPON",
//                "attack":3 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":6 as Int8,
//                "dbfId":424,
//                "flavor":"If she threatens to \"moon\" you, it's not what you think.",
//                "name":"Priestess of Elune",
//                "rarity":"COMMON",
//                "text":"<b>Battlecry:</b> Restore 4 Health to your hero.",
//                "type":"MINION",
//                "attack":5 as Int8,
//                "health":4 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"MAGE",
//                "cost":4 as Int8,
//                "dbfId":430,
//                "flavor":"Magi of the Kirin Tor were casting Cubes of Cold for many years before Cones came into fashion some 90 years ago.",
//                "name":"Cone of Cold",
//                "rarity":"COMMON",
//                "text":"<b>Freeze</b> a minion and the minions next to it, and deal $1 damage to them.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PALADIN",
//                "cost":5 as Int8,
//                "dbfId":435,
//                "flavor":"C'mon Molten Giant!!",
//                "name":"Holy Wrath",
//                "rarity":"RARE",
//                "text":"Draw a card and deal damage equal to its Cost.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":9 as Int8,
//                "dbfId":436,
//                "flavor":"Malygos hates it when mortals use magic.  He gets so mad!",
//                "name":"Malygos",
//                "rarity":"LEGENDARY",
//                "text":"<b>Spell Damage +5</b>",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":12 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"HUNTER",
//                "cost":3 as Int8,
//                "dbfId":437,
//                "flavor":"You could summon Misha, Leokk, or Huffer!  Huffer is more trouble than he's worth.",
//                "name":"Animal Companion",
//                "rarity":"FREE",
//                "text":"Summon a random Beast Companion.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":5 as Int8,
//                "dbfId":440,
//                "flavor":"Abominations enjoy Fresh Meat and long walks on the beach.",
//                "name":"Abomination",
//                "rarity":"RARE",
//                "text":"<b>Taunt</b>. <b>Deathrattle:</b> Deal 2\ndamage to ALL characters.",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":4 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":443,
//                "flavor":"Murloc.  It's what's for dinner.",
//                "name":"Hungry Crab",
//                "rarity":"EPIC",
//                "text":"<b>Battlecry:</b> Destroy a Murloc and gain +2/+2.",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":6 as Int8,
//                "dbfId":445,
//                "flavor":"One Insane Rocketeer.   One Rocket full of Explosives.   Infinite Fun.",
//                "name":"Reckless Rocketeer",
//                "rarity":"FREE",
//                "text":"<b>Charge</b>",
//                "type":"MINION",
//                "attack":5 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"MAGE",
//                "cost":2 as Int8,
//                "dbfId":447,
//                "flavor":"This spell is much better than Arcane Implosion.",
//                "name":"Arcane Explosion",
//                "rarity":"FREE",
//                "text":"Deal $1 damage to all enemy minions.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":3 as Int8,
//                "dbfId":453,
//                "flavor":"The Coldlight murlocs reside in the darkest pits of the Abyssal Depths.  So no, there's no getting away from murlocs.",
//                "name":"Coldlight Seer",
//                "rarity":"RARE",
//                "text":"<b>Battlecry:</b> Give your other Murlocs +2 Health.",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"HUNTER",
//                "cost":2 as Int8,
//                "dbfId":455,
//                "flavor":"Why did it have to be snakes?",
//                "name":"Snake Trap",
//                "rarity":"EPIC",
//                "text":"<b>Secret:</b> When one of your minions is attacked, summon three 1/1 Snakes.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":5 as Int8,
//                "dbfId":456,
//                "flavor":"He was <i>this close</i> to piloting a massive juggernaut into Stormwind Harbor. If it weren't for those pesky kids!",
//                "name":"Captain Greenskin",
//                "rarity":"LEGENDARY",
//                "text":"<b>Battlecry:</b> Give your weapon +1/+1.",
//                "type":"MINION",
//                "attack":5 as Int8,
//                "health":4 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"MAGE",
//                "cost":6 as Int8,
//                "dbfId":457,
//                "flavor":"This spell can be very Entertaining.",
//                "name":"Blizzard",
//                "rarity":"RARE",
//                "text":"Deal $2 damage to all enemy minions and <b>Freeze</b> them.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"SHAMAN",
//                "cost":1 as Int8,
//                "dbfId":458,
//                "flavor":"",
//                "name":"Wrath of Air Totem",
//                "rarity":"FREE",
//                "text":"<b>Spell Damage +1</b>",
//                "type":"MINION",
//                "attack":0 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":1 as Int8,
//                "dbfId":459,
//                "flavor":"Rogues guard the secrets to poison-making carefully, lest magi start incorporating poison into their spells.  Poisonbolt? Rain of Poison?  Poison Elemental?  Nobody wants that.",
//                "name":"Deadly Poison",
//                "rarity":"FREE",
//                "text":"Give your weapon +2 Attack.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":2 as Int8,
//                "dbfId":460,
//                "flavor":"",
//                "name":"Gnoll",
//                "rarity":"",
//                "text":"<b>Taunt</b>",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":2 as Int8,
//                "dbfId":461,
//                "flavor":"Rogues love sappy movies.",
//                "name":"Sap",
//                "rarity":"FREE",
//                "text":"Return an enemy minion to your opponent's hand.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PALADIN",
//                "cost":1 as Int8,
//                "dbfId":462,
//                "flavor":"Justice sometimes takes the form of a closed fist into a soft cheek.",
//                "name":"Eye for an Eye",
//                "rarity":"COMMON",
//                "text":"<b>Secret:</b> When your hero takes damage, deal that much damage to the enemy hero.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":3 as Int8,
//                "dbfId":466,
//                "flavor":"Having Arcane golems at home really classes up the place, and as a bonus they are great conversation pieces.",
//                "name":"Arcane Golem",
//                "rarity":"RARE",
//                "text":"<b>Battlecry:</b> Give your opponent a Mana Crystal.",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":4 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":0 as Int8,
//                "dbfId":467,
//                "flavor":"\"Cast Moonfire, and never stop.\" - How to Be a Druid, Chapter 5, Section 3",
//                "name":"Moonfire",
//                "rarity":"FREE",
//                "text":"Deal $1 damage.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"WARLOCK",
//                "cost":1 as Int8,
//                "dbfId":469,
//                "flavor":"Imps are content to hide and viciously taunt everyone nearby.",
//                "name":"Blood Imp",
//                "rarity":"COMMON",
//                "text":"[x]  <b>Stealth</b>. At the end of your  \nturn, give another random\n friendly minion +1 Health.",
//                "type":"MINION",
//                "attack":0 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":471,
//                "flavor":"",
//                "name":"Damaged Golem",
//                "rarity":"COMMON",
//                "text":"",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PALADIN",
//                "cost":2 as Int8,
//                "dbfId":472,
//                "flavor":"",
//                "name":"Reinforce",
//                "rarity":"FREE",
//                "text":"<b>Hero Power</b>\nSummon a 1/1 Silver Hand Recruit.",
//                "type":"HERO_POWER",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":475,
//                "flavor":"This guy gets crazy strong at family reunions.",
//                "name":"Murloc Tidecaller",
//                "rarity":"RARE",
//                "text":"Whenever you summon a Murloc, gain +1 Attack.",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PALADIN",
//                "cost":4 as Int8,
//                "dbfId":476,
//                "flavor":"Consecrated ground glows with Holy energy.  But it smells a little, too.",
//                "name":"Consecration",
//                "rarity":"FREE",
//                "text":"Deal $2 damage to all enemies.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"PRIEST",
//                "cost":2 as Int8,
//                "dbfId":479,
//                "flavor":"",
//                "name":"Lesser Heal",
//                "rarity":"FREE",
//                "text":"<b>Hero Power</b>\nRestore #2 Health.",
//                "type":"HERO_POWER",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":1 as Int8,
//                "dbfId":481,
//                "flavor":"It is true that some druids are savage, but others still enjoy a quiet moment and a spot of tea.",
//                "name":"Savagery",
//                "rarity":"RARE",
//                "text":"Deal damage equal to your hero's Attack to a minion.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":1 as Int8,
//                "dbfId":482,
//                "flavor":"",
//                "name":"Squire",
//                "rarity":"COMMON",
//                "text":"",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":2 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":1 as Int8,
//                "dbfId":485,
//                "flavor":"",
//                "name":"Wicked Knife",
//                "rarity":"FREE",
//                "text":"",
//                "type":"WEAPON",
//                "attack":1 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"ROGUE",
//                "cost":1 as Int8,
//                "dbfId":488,
//                "flavor":"",
//                "name":"Defias Bandit",
//                "rarity":"",
//                "text":"",
//                "type":"MINION",
//                "attack":2 as Int8,
//                "health":1 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DREAM",
//                "cost":4 as Int8,
//                "dbfId":489,
//                "flavor":"",
//                "name":"Emerald Drake",
//                "rarity":"",
//                "text":"",
//                "type":"MINION",
//                "attack":7 as Int8,
//                "health":6 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":2 as Int8,
//                "dbfId":490,
//                "flavor":"",
//                "name":"Hidden Gnome",
//                "rarity":"COMMON",
//                "text":"Was hiding in a barrel!",
//                "type":"MINION",
//                "attack":1 as Int8,
//                "health":3 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"DRUID",
//                "cost":5 as Int8,
//                "dbfId":493,
//                "flavor":"\"I think I'll just nap under these trees. Wait... AAAAAHHH!\" - Blinkfizz, the Unfortunate Gnome",
//                "name":"Force of Nature",
//                "rarity":"EPIC",
//                "text":"Summon three 2/2 Treants.",
//                "type":"SPELL",
//                "attack":0 as Int8,
//                "health":0 as Int8
//                ])!
//            _ = Card(json:[
//                "cardClass":"NEUTRAL",
//                "cost":5 as Int8,
//                "dbfId":496,
//                "flavor":"The Frostwolves are locked in combat with the Stormpike Expedition over control of Alterac Valley.  Every attempt at peace-talks has ended with Captain Galvangar killing the mediator.",
//                "name":"Frostwolf Warlord",
//                "rarity":"FREE",
//                "text":"<b>Battlecry:</b> Gain +1/+1 for each other friendly minion on the battlefield.",
//                "type":"MINION",
//                "attack":4 as Int8,
//                "health":4 as Int8
//                ])!
//            
//        }
//        
//    }



}
