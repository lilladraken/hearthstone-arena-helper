//
//  RestConnection.swift
//  arena_forge
//
//  Created by Paul Weston on 23/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import Foundation
import UserNotifications


class RestConnection{

  // Session
  let session = URLSession.shared
  
  // REST URLS
  let BASE_URL: String = "https://omgvamp-hearthstone-v1.p.mashape.com/cards?collectible=1"
  let API_KEY: String = "vXJ3VCpO6fmshg7Mo5eG3bo0FTHvp1vImBtjsnuWezqgE2lGxa"
  let HEADER: String = "X-Mashape-Key"
  
  // Singleton Variable and Functions
  private init(){
  
  }
  
  fileprivate struct Static
  {
    static var instance: RestConnection?
  }
  
  class var sharedInstance: RestConnection
  {
    if (Static.instance == nil)
    {
      Static.instance = RestConnection()
    }
    return Static.instance!
  }
  
  
  // Get Card Data from REST Service
  func getCardData(firstLoad: Bool){
    if let url = URL(string: BASE_URL){
      var request = URLRequest(url: url)
      request.setValue(API_KEY, forHTTPHeaderField: HEADER)
      initialiseTaskForGettingData(request, element: "Classic", firstLoad: firstLoad)
    }
  }
  
  // Set up Sessions Connection and Make Request
  func initialiseTaskForGettingData(_ request: URLRequest, element:String, firstLoad: Bool)
  {
    
    let task = session.dataTask(with: request, completionHandler: {data, response, downloadError in
      if let error = downloadError
      {
        print("\(String(describing: data)) \n data")
        print("\(String(describing: response)) \n response")
        print("\(error)\n error")
      }
      else
      {
        let parsedResult: Any!
        do
        {
          // Convert the http response payload to JSON.
          parsedResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
        }
        catch _ as NSError
        {
          parsedResult = nil
        }
        catch
        {
          fatalError()
        }
        
        // Store all Cards and Sets in array
        if let cardSetArray = (parsedResult as? [String: NSArray]){
        
          // Loop through the Cardsets
          for cs in cardSetArray
          {
            // Loop through each card in each set
            for c in cs.value
            {
              let card = c as! NSDictionary
              
              // Parse card Data, if specific data is not present set default values
              let playerClass = card.value(forKey: "playerClass") as! String
              let cost = card.value(forKey: "cost") as? Int16 ?? 0 as (Int16)
              let dbfId = Int32(card.value(forKey: "dbfId") as! String)!
              let img = card.value(forKey: "img") as? String ?? "" as String
              let flavor = card.value(forKey: "flavor") as? String ?? "" as String
              let name = card.value(forKey: "name") as! String
              let rarity = card.value(forKey: "rarity") as? String ?? "" as String
              let text = card.value(forKey: "text") as? String ?? "" as String
              let type = card.value(forKey: "type") as? String ?? "" as String
              let attack = card.value(forKey: "attack") as? Int16 ?? 0 as (Int16)
              let health = card.value(forKey: "health") as? Int16 ?? 0 as (Int16)
              
              // Check if the Card from Rest Service already exists locally
              // If Card Data from rest service does not match the exists card data the card is updated
              if !firstLoad{
                if let existingCard = Card.sharedInstance.getCardById(id: dbfId){
                  if existingCard.name != name ||
                     existingCard.attack != attack ||
                     existingCard.cost != cost ||
                     existingCard.health != health ||
                     existingCard.cardClass != playerClass ||
                     existingCard.text != text ||
                     existingCard.flavor != flavor ||
                     existingCard.rarity != rarity ||
                     existingCard.type != type ||
                     existingCard.imageURL != img{
                     DispatchQueue.main.async {Card.sharedInstance.saveCard(id: dbfId,
                                                                            name: name,
                                                                            attack: attack,
                                                                            cost: cost,
                                                                            health: health,
                                                                            cardClass: playerClass,
                                                                            text: text,
                                                                            flavor: flavor,
                                                                            rarity: rarity,
                                                                            type: type,
                                                                            imageURL: img,
                                                                            existing: existingCard)}
                      print("\(existingCard.name!) Updated!")
                  }
                }
                else if img != ""{
                  DispatchQueue.main.async {Card.sharedInstance.saveCard(id: dbfId,
                                                                         name: name,
                                                                         attack: attack,
                                                                         cost: cost,
                                                                         health: health,
                                                                         cardClass: playerClass,
                                                                         text: text,
                                                                         flavor: flavor,
                                                                         rarity: rarity,
                                                                         type: type,
                                                                         imageURL: img,
                                                                         existing: nil)}
                }
              }
              else if img != ""{
                DispatchQueue.main.async {Card.sharedInstance.saveCard(id: dbfId,
                                                                       name: name,
                                                                       attack: attack,
                                                                       cost: cost,
                                                                       health: health,
                                                                       cardClass: playerClass,
                                                                       text: text,
                                                                       flavor: flavor,
                                                                       rarity: rarity,
                                                                       type: type,
                                                                       imageURL: img,
                                                                       existing: nil)}
              }
            }
          }
          // Notify the observer that the card loading from the rest service ass finished
          let name = Notification.Name(rawValue: cardLoadFinishedNotificationKey)
          DispatchQueue.main.async {NotificationCenter.default.post(name: name, object: nil)}
        }
        
      }
    })
    // Execute the task
    task.resume()
  }

}
