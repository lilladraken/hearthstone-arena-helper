//
//  Deck+CoreDataProperties.swift
//  arena_forge
//
//  Created by Paul Weston on 26/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import Foundation
import CoreData


extension Deck {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Deck> {
        return NSFetchRequest<Deck>(entityName: "Deck")
    }

    @NSManaged public var deckName: String?
    @NSManaged public var deckDate: NSDate?
    @NSManaged public var deckHeroClass: String?
    @NSManaged public var deckCardIDs: NSObject?

}
