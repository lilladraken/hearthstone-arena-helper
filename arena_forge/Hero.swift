//
//  Hero.swift
//  arena_forge
//
//  Created by Paul Weston on 3/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import Foundation

enum Hero: Int {
  
  //Hero Cases
  case warrior = 1, shaman, rogue, paladin, hunter, druid, warlock, mage, priest
  
  //Default initialiser if hero is not asked for explicitly.
  init() {
    self = .warrior
  }
  
  init?(number: Int) {
    switch number {
      case 1: self = .warrior
      case 2: self = .shaman
      case 3: self = .rogue
      case 4: self = .paladin
      case 5: self = .hunter
      case 6: self = .druid
      case 7: self = .warlock
      case 8: self = .mage
      case 9: self = .priest
      default: return nil
    }
  }
  
  // Return Hero Rank
  var rank: Int {
    get {
      return self.rawValue
    }
  }
  
  // Class Name of the Hero
  var className:String {
    get {
      switch self {
        case .warrior: return "Warrior"
        case .shaman: return "Shaman"
        case .rogue: return "Rogue"
        case .paladin: return "Paladin"
        case .hunter: return "Hunter"
        case .druid: return "Druid"
        case .warlock: return "Warlock"
        case .mage: return "Mage"
        case .priest: return "Priest"
      }
    }
  }
  
  // Heroes Actual Name
  var heroName:String {
    get {
      switch self {
        case .warrior: return "Garrosh Hellscream"
        case .shaman: return "Thrall"
        case .rogue: return "Valeera Sanquinar"
        case .paladin: return "Uther Lightbringer"
        case .hunter: return "Alleria Windrunner"
        case .druid: return "Malfurion Stormrage"
        case .warlock: return "Gul'dan"
        case .mage: return "Jaina Proudmoore"
        case .priest: return "Anduin Wrynn"
      }
    }
  }
  
  //The Computed image Name of the hero
  var imageName:String {
    get {
      return "hero-" + self.className.lowercased()
    }
  }
  
  //returns the full hero list
  static func getHeroList() -> [Hero] {
    return [warrior, shaman, rogue, paladin, hunter, druid, warlock, mage, priest]
  }
  
}
