//
//  LoginViewController.swift
//  arena_forge
//
//  Created by Paul Weston on 24/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

  // Activity Outlets
  @IBOutlet weak var overlayView: UIView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  // Text Field Outlets
  @IBOutlet weak var usernameField: UITextField!
  @IBOutlet weak var passwordField: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Keyboard Dismissal when tapping outside keyboard
    self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view,
                                                          action: #selector(UIView.endEditing(_:))))
    startActivityIndicator()
  }

  override func viewDidAppear(_ animated: Bool) {
    // Set Observer
    let name = Notification.Name(rawValue: cardLoadFinishedNotificationKey)
    NotificationCenter.default.addObserver(self, selector: #selector(stopActivityIndicator), name: name, object: nil)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    // Reset Text Field text
    usernameField.text = ""
    passwordField.text = ""
  }
  
  //Login Button Tapped
  @IBAction func loginButtonTapped(_ sender: Any) {
    // If a user profile exists check the username and password entered
    if Profile.sharedInstance.profiles.count != 0{
      if let profile = Profile.sharedInstance.getProfileByUsername(username: usernameField.text!){
        if profile.password == passwordField.text{
          Profile.sharedInstance.setCurrentUserProfile(user: profile)
          performSegue(withIdentifier: "login", sender: nil)
        }
        else{
          // if user types in the wrong password, present the Wrong Information Alert
          wrongInfoAlert()
        }
      }
      else{
        // if user types in the wrong username, present the Wrong Information Alert
        wrongInfoAlert()
      }
    }
    // if no profile exists present the Wrong Information Alert
    else{
      wrongInfoAlert()
    }
  }
  
  // Alert for when the wrong user information is entered
  func wrongInfoAlert(){
    
    let alert = UIAlertController(title: "Failed Username or Password", message: "Oh no! You have typed the wrong username or password", preferredStyle: UIAlertControllerStyle.alert)
    
    let cancelAction = UIAlertAction(title: "Try Again",
                                     style: .cancel,
                                     handler: nil)
    
    alert.addAction(cancelAction)
    self.present(alert, animated: true)
  }
  
  // Start the load indicator
  func startActivityIndicator(){
    overlayView.isHidden = false
    activityIndicator.startAnimating()
  }
  
  // Stop the load indicator when card information has finished loading
  func stopActivityIndicator(){
    overlayView.isHidden = true
    activityIndicator.stopAnimating()
  }
  
  // Unwind from Create Profile
  @IBAction func unwindToLogin(segue: UIStoryboardSegue) {
  }
  
  
  // Make status bar light
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
  }

}
