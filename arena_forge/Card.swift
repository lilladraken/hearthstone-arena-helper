//
//  Card.swift
//  arena_forge
//
//  Created by Steph Ridnell on 27/12/17.
//  Copyright © 2017 Taylor Swift. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Card {
  
  // Get a reference to your App Delegate
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
  
  // Get a database context from the app delegate
  var managedContext: NSManagedObjectContext
  {
    get {
      return appDelegate.persistentContainer.viewContext
    }
  }
  
  // All Cards in Single Array
  var allCards = [NSManagedObject]()
  
  // Get All Cards from Core Data
  private func getCardsFromCoreData()
  {
    do
    {
      let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"CardCD")
      
      let results =
        try managedContext.fetch(fetchRequest)
      allCards = results as! [NSManagedObject]
    }
    catch let error as NSError
    {
      print("Could not fetch \(error), \(error.userInfo)")
    }
  }
  
  // Save Card Data
  func saveCard(id: Int32, name: String, attack: Int16, cost: Int16, health: Int16, cardClass: String,
                text: String, flavor: String, rarity: String, type: String, imageURL: String, existing: CardCD?)  {
    let entity =
      NSEntityDescription.entity(forEntityName: "CardCD",
                                 in: managedContext)!
    
    // If existing is not nil then update existing Card Data
    if let _ = existing
    {
      existing!.id = id
      existing!.name = name
      existing!.attack = attack
      existing!.cost = cost
      existing!.health = health
      existing!.cardClass = cardClass
      existing!.text = text
      existing!.flavor = flavor
      existing!.rarity = rarity
      existing!.type = type
      existing!.imageURL = imageURL
    }
      // Create a new Card object and update it with the data passed in from the View Controller
    else
    {
      // Create new Card
      let card = NSManagedObject(entity: entity,
                                 insertInto: managedContext)
      
      card.setValue(id, forKeyPath: "id")
      card.setValue(name, forKeyPath: "name")
      card.setValue(attack, forKeyPath: "attack")
      card.setValue(cost, forKeyPath: "cost")
      card.setValue(health, forKeyPath: "health")
      card.setValue(cardClass, forKeyPath: "cardClass")
      card.setValue(text, forKeyPath: "text")
      card.setValue(flavor, forKeyPath: "flavor")
      card.setValue(rarity, forKeyPath: "rarity")
      card.setValue(type, forKeyPath: "type")
      card.setValue(imageURL, forKeyPath: "imageURL")
      allCards.append(card)
    }
    updateDatabase()
  }
  
  // Save the current state of the objects in the managed context into the database.
  func updateDatabase()
  {
    do
    {
      try managedContext.save()
    }
    catch let error as NSError
    {
      print("Could not save \(error), \(error.userInfo)")
    }
  }
  
  
  // Singleton Functions
  private init()
  {
    getCardsFromCoreData()
  }
  
  fileprivate struct Static
  {
    static var instance: Card?
  }
  
  
  class var sharedInstance: Card
  {
    if (Static.instance == nil)
    {
      Static.instance = Card()
    }
    return Static.instance!
  }

  
  // Getter for all cards, returns nil if no cards found
  func getAllCards() -> [CardCD]?{
    
    if allCards.isEmpty {
      print("No cards found")
      return nil
    }
    
    return allCards as? [CardCD]
    
  }

  //returns all cards in a Class, returns nil if no cards found
  func getCardsByClass(className:String) -> [CardCD]? {
    
    var classCards:[CardCD] = []
    
    for c in allCards {
      let card = c as! CardCD
      if card.cardClass?.lowercased() == className.lowercased() {
        classCards.append(card)
      }
      
    }
    
    if classCards.isEmpty {
      print("No cards found for that class: " + className)
      return nil
    }
    
    return classCards
    
  }

  //Returns the card if it matches the id, otherwise it returns nil
  func getCardById(id: Int32) -> CardCD? {
    
    for c in allCards {
      let card = c as! CardCD
      if card.id == id {
        return card
      }
    }
    
    print("Card does not exist with id: \(id)")
    return nil
    
  }
  
  //Returns the card if it matches the id, otherwise it returns nil
  func getCardByName(name: String) -> CardCD? {
    
    for c in allCards {
      let card = c as! CardCD
      if card.name == name {
        return card
      }
    }
    
    print("Card does not exist with id: \(name)")
    return nil
    
  }
  
  
}
