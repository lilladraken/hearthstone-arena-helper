//
//  DeckManaCurve.swift
//  arena_forge
//
//  Created by Paul Weston on 31/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import Foundation


class DeckManaCurve{

  var deck: DeckCD?
  var cards:[CardCD] = []
  
  // Mana Values for Sorting
  var manaValues: [Int] = [0,0,0,0,0,0,0,0]
  
  // Type Values for Sorting
  var typeValues: [String:Int] = ["Minions": 0,
                                  "Spells": 0,
                                  "Weapons": 0,
                                  "Battlecry": 0,
                                  "Overload": 0,
                                  "Taunt": 0,
                                  "DeathRattle": 0]
  
  init(deck: DeckCD){
    self.deck = deck
    loadCards()
    sortManaValues()
    sortTypeValues()
  }
  
  // Getter for type value
  func getTypeValue(_ type: String) -> String{
    let value: Int = typeValues[type]!
    return String(value)
  }
  
  // Get the Cards IDs from the Deck Clasd and load them into the local array
  private func loadCards(){
    // Gets deck cards
    let deckCards = deck?.getCardIds()
    for i in deckCards!{
      cards.append(Card.sharedInstance.getCardById(id: i)!)
    }
  }
  
  
  //Sort the Mana Cost in the Cost Array
  private func sortManaValues(){
    for i in cards{
      switch i.cost{
      case 0: manaValues[0] += 1
      case 1: manaValues[1] += 1
      case 2: manaValues[2] += 1
      case 3: manaValues[3] += 1
      case 4: manaValues[4] += 1
      case 5: manaValues[5] += 1
      case 6: manaValues[6] += 1
      case 7: manaValues[7] += 1
      default: manaValues[7] += 1
        
      }
    }
    
  }
  
  // Sort the Types of the cards into the Types Array
  private func sortTypeValues(){
    for i in cards{
      if i.getText().lowercased().contains("minion:") || i.getType().lowercased().contains("minion"){
        typeValues["Minions"] = typeValues["Minions"]! + 1
      }
      if i.getText().lowercased().contains("spell:") || i.getType().lowercased().contains("spell"){
        typeValues["Spells"] = typeValues["Spells"]! + 1
      }
      if i.getText().lowercased().contains("weapon:") || i.getType().lowercased().contains("weapon"){
        typeValues["Weapons"] = typeValues["Weapons"]! + 1
      }
      if i.getText().lowercased().contains("battlecry:") || i.getType().lowercased().contains("battlecry"){
        typeValues["Battlecry"] = typeValues["Battlecry"]! + 1
      }
      if i.getText().lowercased().contains("overload:") || i.getType().lowercased().contains("overload"){
        typeValues["Overload"] = typeValues["Overload"]! + 1
      }
      if i.getText().lowercased().contains("taunt:") || i.getType().lowercased().contains("taunt"){
        typeValues["Taunt"] = typeValues["Taunt"]! + 1
      }
      if i.getText().lowercased().contains("deathrattle:") || i.getType().lowercased().contains("deathrattle"){
        typeValues["DeathRattle"] = typeValues["DeathRattle"]! + 1
      }
    }
  }




}
