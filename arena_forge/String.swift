//
//  String.swift
//  arena_forge
//
//  Created by Paul Weston on 4/2/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import Foundation

extension String {

  // Removes Special Characters and HTML Tags
  func removeSpecialChars() -> String {
    
    var sanatizedString = ""
    
    // Remove the HTML Tags
    sanatizedString = self.replacingOccurrences(of: "<[^>]*>", with: "", options: .regularExpression, range: nil)
    
    //Remove Special Characters
    let okayChars : Set<Character> = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_<>".characters)
    return String(sanatizedString.characters.filter {okayChars.contains($0) })
  }
  
}
