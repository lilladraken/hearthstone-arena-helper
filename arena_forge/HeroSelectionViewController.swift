//
//  HeroSelectionViewController.swift
//  arena_forge
//
//  Created by Paul Weston on 29/12/17.
//  Copyright © 2017 Taylor Swift. All rights reserved.
//

import UIKit

class HeroSelectionViewController: UIViewController {
  
  //List of All Heroes
  var heroList:[Hero]?
  
  //Alpha Value for hero image while inactive
  var inactiveAlpha:CGFloat = 0.5
  
  //Hero Selection Collection View
  @IBOutlet weak var heroCollectionView: UICollectionView!
  
  //Next Button Reference
  @IBOutlet weak var nextButton: UIButton!
  
  var selectedHero: Hero?
  
  // Deck for continuing to add to deck
  private var rebuildDeck: DeckCD?
  private var continueDeckBuild = false
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    heroList = Hero.getHeroList()
    
    //Setup delegate and datasource for collection view
    heroCollectionView.delegate = self
    heroCollectionView.dataSource = self
    
  }
  
  
  override func viewDidAppear(_ animated: Bool) {
    
    // Reset Hero Selection when the user pops to the root view controller
    for cell in heroCollectionView.visibleCells as [UICollectionViewCell] {
      if let imageView = cell.viewWithTag(1) as? UIImageView {
        imageView.alpha = inactiveAlpha
      }
    }
    
    //Reset Hero Selection process
    selectedHero = nil
    nextButton.isEnabled = false
    nextButton.alpha = inactiveAlpha
    
    // Segues to Arena when continuing to add to a Deck
    if continueDeckBuild{
      continueBuild()
    }
  }
  
  
  override func viewWillAppear(_ animated: Bool) {
    // Fixes Bug that is caused by the Image Picker in landscape
    if UIDevice.current.orientation.isLandscape{
      heroCollectionView.reloadData()
    }
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // Reset all Hero Image Alphas back to original value
  // Selected cells alpha set to 1
  fileprivate func resetHeroImageAlphas(collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    for cell in collectionView.visibleCells as [UICollectionViewCell] {
      if let imageView = cell.viewWithTag(1) as? UIImageView {
        
        if collectionView.indexPath(for: cell) == indexPath {
          imageView.alpha = 1
        } else {
          imageView.alpha = inactiveAlpha
        }
      }
    }
  }
  
  
  // Activates next button and set alpha to 1
  fileprivate func activateNextButton() {
    
    if nextButton.isEnabled == false {
      nextButton.isEnabled = true
      nextButton.alpha = 1
    }
    
  }
  
  
  // Sending the selected Hero to the Arena Selection View Controller
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    let arenaVC: ArenaSelectionViewController = segue.destination as! ArenaSelectionViewController
    
    // Sends the Hero to the Arena View Controller when starting a Deck
    if selectedHero != nil{
      arenaVC.selectedHero = self.selectedHero
    }
    
    // Sends the Rebuild Deck to the Arena View Controller when continuing a Deck
    else if rebuildDeck != nil{
      arenaVC.currentDeck = self.rebuildDeck
    }
  }

  
  // Sets the Rebuild deck when continuing to add to a deck
  func setRebuildDeck(deck: DeckCD){
    self.rebuildDeck = deck
    continueDeckBuild = true
  }
  
  
  // Segue to the Hero Selection Scene when continuing to add to a deck
  private func continueBuild(){
    continueDeckBuild = false
    performSegue(withIdentifier: "HeroToArena", sender: nil)
  }
  
}



// Collection View Specfic Functions

extension HeroSelectionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  
  // generates the number of cells in the collection view
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return (heroList?.count)!
  }
  
  // adds cells to the collection view
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    // setup cells based on prototype in storyboard
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeroSelectionCell", for: indexPath)
    
    // Assign images to the current cell
    if let imageView = cell.viewWithTag(1) as? UIImageView {
      imageView.image = UIImage(named: (heroList?[indexPath.row].imageName)!)
      imageView.alpha = inactiveAlpha
    }
    
    // round corners of cell
    cell.layer.cornerRadius = 10
    cell.clipsToBounds = true
    return cell
  }
  
  // Handles when the cell is tapped.
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    resetHeroImageAlphas(collectionView: collectionView, didSelectItemAt: indexPath)
    selectedHero = heroList?[indexPath.row]
    activateNextButton()
    
  }
  
  // Sets the size of the cell based on the size of the view controller
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
    let collectionViewHeight = heroCollectionView.frame.height
    
    var height = collectionView.cellForItem(at: indexPath)?.frame.height
    
    if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
      
      let cellSpacing = layout.minimumLineSpacing

      height = (collectionViewHeight-(cellSpacing*2))/3
        
    }
      
    return CGSize(width: height!,height: height!)

  }
  
  // reload the collection view when the device rotates
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    heroCollectionView.reloadData()
  }
  
 
}
