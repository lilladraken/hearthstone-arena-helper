//
//  UserProfile+CoreDataProperties.swift
//  arena_forge
//
//  Created by Paul Weston on 27/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import Foundation
import CoreData


extension UserProfile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserProfile> {
        return NSFetchRequest<UserProfile>(entityName: "UserProfile")
    }

    @NSManaged public var userName: String?
    @NSManaged public var password: String?
    @NSManaged public var favHero: String?
    @NSManaged public var email: String?
    @NSManaged public var image: NSData?
    @NSManaged public var userId: Int64

}
