//
//  Deck.swift
//  arena_forge
//
//  Created by Paul Weston on 6/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Deck{
  
  // Get a reference to your App Delegate
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
  
  // Get a database context from the app delegate
  var managedContext: NSManagedObjectContext
  {
    get {
      return appDelegate.persistentContainer.viewContext
    }
  }
  
  // Create a collection of objects to store in the database
  var allDecks = [NSManagedObject]()

  
  // Max limit of cards that can be in the deck
  let cardLimit: Int = 30
  
  
  // Checks if there is any Decks available
  var noDecksAvailable: Bool{
    return allDecks.isEmpty
  }
  
  // Gets all decks from core data and loads then into the all decks array
  private func getDecksFromCoreData()
  {
    do
    {
      let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"DeckCD")
      
      let results =
        try managedContext.fetch(fetchRequest)
      allDecks = results as! [NSManagedObject]
    }
    catch let error as NSError
    {
      print("Could not fetch \(error), \(error.userInfo)")
    }
  }
  
  // Save new decks and Update existing decks
  func saveDeck(deckName: String?, deckHeroClass: String, cardIDs: [Int32], existing: DeckCD?) -> DeckCD?
  {
    var newDeck: DeckCD?
    
    let entity =
      NSEntityDescription.entity(forEntityName: "DeckCD",
                                 in: managedContext)!
    
    // If existing is not nil then update existing movie
    if let _ = existing
    {
      existing!.deckCardIDs = cardIDs
      if deckName != nil{
        existing!.deckName = deckName
      }
      existing!.deckHeroClass = deckHeroClass
    }
      // Create a new Deck object and update it with the data passed in from the View Controller
    else
    {
      // Create new Deck
      let deck = NSManagedObject(entity: entity,
                                  insertInto: managedContext)
      
      let deckDate = Date()
      let newDeckName: String
      let userId = Profile.sharedInstance.currentProfile?.userId
      
      if deckName == nil{
      // Setup Date Formatter
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-YY"
      
        newDeckName = String("\(deckHeroClass) " + dateFormatter.string(from: deckDate))
      }
      else{
        newDeckName = deckName!
      }
      
      deck.setValue(userId, forKeyPath: "userId")
      deck.setValue(cardIDs, forKeyPath: "deckCardIDs")
      deck.setValue(deckDate, forKeyPath: "deckDate")
      deck.setValue(newDeckName, forKeyPath: "deckName")
      deck.setValue(deckHeroClass, forKeyPath: "deckHeroClass")
      allDecks.append(deck)
      newDeck = deck as? DeckCD
    }
    updateDatabase()
    return newDeck
  }
  
  // Gets specific deck from table reference
  func getDeck(_ indexPath: IndexPath) -> DeckCD
  {
    return allDecks[indexPath.row] as! DeckCD
  }
  
  // Delete Deck
  func deleteDeck(deck: DeckCD)
  {
    for (index,element) in allDecks.enumerated(){
      if let loopDeck = element as? DeckCD{
        if deck.deckDate == loopDeck.deckDate{
          allDecks.remove(at: index)
        }
      }
    }
    managedContext.delete(deck)
    updateDatabase()    
  }
  
  // Save the current state of the objects in the managed context into the database.
  func updateDatabase()
  {
    do
    {
      try managedContext.save()
    }
    catch let error as NSError
    {
      print("Could not save \(error), \(error.userInfo)")
    }
  }
  
  // Singleton Functions
  private init()
  {
    getDecksFromCoreData()
  }
  
  fileprivate struct Static
  {
    static var instance: Deck?
  }
  
  
  class var sharedInstance: Deck
  {
    if (Static.instance == nil)
    {
      Static.instance = Deck()
    }
    return Static.instance!
  }

  
  // Returns All Decks
  func getAllDecks() -> [DeckCD]?{
    if allDecks.isEmpty {
      return nil
    }
    return allDecks as? [DeckCD]
  }
  
  // Returns User Decks
  func getUserDecks() -> [DeckCD]?{
    if allDecks.isEmpty {
      return nil
    }
    
    var decks:[DeckCD] = []
    
    for d in allDecks{
      if let deck = d as? DeckCD{
        if deck.userId == Profile.sharedInstance.currentProfile?.userId{
          decks.append(deck)
        }
      }
    }
    
    return decks
  }
  
  
}
