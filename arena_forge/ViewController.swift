//
//  ViewController.swift
//  arena_forge
//
//  Created by Steph Ridnell on 27/12/17.
//  Copyright © 2017 Taylor Swift. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  // Make status bar light
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}

