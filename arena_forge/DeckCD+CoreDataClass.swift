//
//  DeckCD+CoreDataClass.swift
//  arena_forge
//
//  Created by Paul Weston on 26/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import Foundation
import CoreData

@objc(DeckCD)
public class DeckCD: NSManagedObject {

  
  // Returns the deck cards sorted first in ASC Cost then in alphbetical order of name
  func getSortedCards() -> [CardCD]{
    
    var cards: [CardCD] = []
    
    // Gets deck cards
    if let deckCards = self.deckCardIDs{
      for i in deckCards{
        cards.append(Card.sharedInstance.getCardById(id: i)!)
      }
    
      // Sort cards by cost ASC
      cards = cards.sorted(by: { (c1, c2) -> Bool in
        if c1.cost < c2.cost {
          return true
        } else if c1.cost > c2.cost {
          return false
        } else if c1.name! < c2.name! {
          return true
        }
        return false
      })
    }
      return cards
  }

  // Returns the number of cards in the Deck
  var cardCount: Int {
    return deckCardIDs!.count
  }
  
  //Checks if the deck is full based on count
  var isDeckFull: Bool {
    
    if deckCardIDs!.count < Deck.sharedInstance.cardLimit {
      return false
    } else {
      return true
    }
    
  }
  
  // Add Card to deck
  func addCard(id: Int32) {
    self.deckCardIDs!.append(id)
    saveDeck()
  }
  
  // Returns the ids to all cards in the deck
  func getCardIds() -> [Int32] {
    return self.deckCardIDs!
  }
  
  // Setter for Name
  func setName(_ name: String){
    self.deckName = name
    saveDeck()
  }
  
  // Save Deck
  func saveDeck(){
    _ = Deck.sharedInstance.saveDeck(deckName: self.deckName, deckHeroClass: self.deckHeroClass!, cardIDs: self.deckCardIDs!, existing: self)
  }
  
}
