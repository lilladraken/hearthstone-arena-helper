//
//  StartNewDeckViewController.swift
//  arena_forge
//
//  Created by Paul Weston on 9/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import UIKit

class StartNewDeckViewController: UIViewController {

  override func viewDidLoad() {
      super.viewDidLoad()

      // Do any additional setup after loading the view.
  }

  override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
  }
  
  // Sends the User back to the Hero Selection Scene
  @IBAction func backToStart(_ sender: Any) {
    
    self.navigationController?.popToRootViewController(animated: false)
    
  }

}
