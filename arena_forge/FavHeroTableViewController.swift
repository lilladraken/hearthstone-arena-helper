//
//  FavHeroTableViewController.swift
//  arena_forge
//
//  Created by Paul Weston on 3/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import UIKit

class FavHeroTableViewController: UITableViewController {
  
  //List of heroes to populate the table
  var heroList:[Hero]?
  
  //Name of hero the user selected
  var selectedHero: String?
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    heroList = Hero.getHeroList()
    self.navigationController?.navigationBar.topItem?.backBarButtonItem?.title = " "
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  //Setup Number of Table Rows
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if let heroList = self.heroList{
      return heroList.count
    }
    return 0
  }
  
  //Setup list of Heroes into Table View Cells
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "HeroListCell", for: indexPath)
    if let hero = heroList?[indexPath.row]{
    
      //Set cell label
      cell.textLabel?.text = hero.heroName
      cell.textLabel?.textColor = UIColor.white
      if let imageView = cell.viewWithTag(1) as? UIImageView{
        imageView.image = UIImage(named: hero.imageName)
      }
    }
    return cell
  }
  
  
  //Set the selected hero before unwind to Profile View Controller
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    let indexPath = tableView.indexPathForSelectedRow?.row

    selectedHero = heroList?[indexPath!].heroName
  }
  
}
