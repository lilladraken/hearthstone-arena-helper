//
//  ArenaSelectionViewController.swift
//  arena_forge
//
//  Created by Paul Weston on 1/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import UIKit

class ArenaSelectionViewController: UIViewController {
  
  //Selected Hero
  var selectedHero: Hero?
  
  //Selected Cards
  var topCard: CardCD?
  var middleCard: CardCD?
  var bottomCard: CardCD?
  
  var activePair:[UIView] = []
  var activeCard: String = ""
  
  //Selected Card Views
  @IBOutlet weak var topSelectedCardView: UIView!
  @IBOutlet weak var middleSelectedCardView: UIView!
  @IBOutlet weak var bottomSelectedCardView: UIView!
  
  //Empty Card Views
  @IBOutlet weak var topEmptyCardView: UIView!
  @IBOutlet weak var middleEmptyCardView: UIView!
  @IBOutlet weak var bottomEmptyCardView: UIView!
  
  //Side Menu Variables
  @IBOutlet weak var screenCoverButton: UIButton!
  var sideMenuOpen = false
  @IBOutlet weak var sideMenuTrailingConstriant: NSLayoutConstraint!
  @IBOutlet weak var sideMenuView: UIView!
  @IBOutlet weak var viewDeckButton: UIButton!
  
  //Current Deck
  var currentDeck: DeckCD?
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.navigationItem.hidesBackButton = true
    self.tabBarController?.delegate = self
    
    if currentDeck == nil {      
      self.currentDeck = Deck.sharedInstance.saveDeck(deckName: nil, deckHeroClass: selectedHero!.className, cardIDs: [], existing: nil)
    }
  }

  
  // Refresh Tabbar delegate
  override func viewDidAppear(_ animated: Bool) {
    self.tabBarController?.delegate = self
    updateViewDeckLabel()
  }
  
  
  //SEARCH BUTTONS
  
  // Top Search Button Actions
  @IBAction func topSearchButton(_ sender: Any) {
    performSegue(withIdentifier: "cardSearch", sender: [topSelectedCardView,topEmptyCardView])
    activeCard = "top"
  }
  
  // Middle Search Button Actions
  @IBAction func middleSearchButton(_ sender: Any) {
    performSegue(withIdentifier: "cardSearch", sender: [middleSelectedCardView,middleEmptyCardView])
    activeCard = "middle"
  }
  
  // Bottom Search Button Actions
  @IBAction func bottomSearchButton(_ sender: Any) {
    performSegue(withIdentifier: "cardSearch", sender: [bottomSelectedCardView,bottomEmptyCardView])
    activeCard = "bottom"
  }
  
  
  
  //REMOVE BUTTONS
  @IBAction func topRemoveButton(_ sender: Any) {
    topSelectedCardView.isHidden = true
    topEmptyCardView.isHidden = false
  }
  
  @IBAction func middleRemoveButton(_ sender: Any) {
    middleSelectedCardView.isHidden = true
    middleEmptyCardView.isHidden = false
  }
  
  @IBAction func bottomRemoveButton(_ sender: Any) {
    bottomSelectedCardView.isHidden = true
    bottomEmptyCardView.isHidden = false
  }
  
  
  
  //ADD BUTTONS
  @IBAction func topAddButton(_ sender: Any) {
    currentDeck?.addCard(id: (topCard?.id)!)
    resetCardViews()
    
    if (currentDeck?.isDeckFull)! {
      performSegue(withIdentifier: "selectionCompleted", sender: nil)
    }
    
    updateViewDeckLabel()
  }
  
  @IBAction func middleAddButton(_ sender: Any) {
    currentDeck?.addCard(id: (middleCard?.id)!)
    resetCardViews()
    
    if (currentDeck?.isDeckFull)! {
      performSegue(withIdentifier: "selectionCompleted", sender: nil)
    }
    
    updateViewDeckLabel()
  }
  
  @IBAction func bottomAddButton(_ sender: Any) {
    currentDeck?.addCard(id: (bottomCard?.id)!)
    resetCardViews()
    
    if (currentDeck?.isDeckFull)! {
      performSegue(withIdentifier: "selectionCompleted", sender: nil)
    }
    
    updateViewDeckLabel()
  }
  
  
  //Resets the cards views to all be blank
  func resetCardViews(){
    
    topSelectedCardView.isHidden = true
    topEmptyCardView.isHidden = false
    middleSelectedCardView.isHidden = true
    middleEmptyCardView.isHidden = false
    bottomSelectedCardView.isHidden = true
    bottomEmptyCardView.isHidden = false
    
  }
  
  
  //Update the View Deck Label.
  func updateViewDeckLabel(){
    viewDeckButton.setTitle("View Deck: (" +
      String((currentDeck?.cardCount)!) + "/" +
      String(Deck.sharedInstance.cardLimit) + ")",
                            for: .normal)
  }
  
  
  //Populates the card details into the passed view
  func populateCardDetailsToView(cardView: UIView, emptyCardView: UIView, card: CardCD) -> Bool{
    
    //Card Data Placeholders are referenced by tag set in the storyboard view
    //tag numbers are as follows
    //Card Image = 1
    //Card Name = 2
    //Mana Cost = 3
    //Attack Value = 4
    //Health Value = 5
    //Card Score = 6
    
    //Change the Card Image with selected image
    if let cardImage = cardView.viewWithTag(1) as? UIImageView{
      
      DispatchQueue.main.async(execute: {cardImage.image = card.getImage()})
      
    } else {
      return false
    }
    
    //Update Card Name
    if let nameLabel = cardView.viewWithTag(2) as? UILabel{
      nameLabel.text = card.name
    } else {
      return false
    }
    
    //Update Mana Cost
    if let manaCost = cardView.viewWithTag(3) as? UILabel{
      manaCost.text = String(card.cost)
    } else {
      return false
    }
    
    //Update Attack Value
    if let attackLabel = cardView.viewWithTag(4) as? UILabel{
      attackLabel.text = String(card.attack)
    } else {
      return false
    }
    
    //Update Health Value
    if let healthLabel = cardView.viewWithTag(5) as? UILabel{
      healthLabel.text = String(card.health)
    } else {
      return false
    }
    
    //Update Score Value
    if let scoreLabel = cardView.viewWithTag(6) as? UILabel{
      
      let score = card.getRank(heroClass:  (currentDeck?.deckHeroClass)!)
      
      //Set the Label color based on the card score
      if score < 30{
        scoreLabel.textColor = UIColor.red
      }
      else if score < 70 {
        scoreLabel.textColor = UIColor.orange
      }
      else{
        //Same Green Colour as the add Button
        scoreLabel.textColor = UIColor.init(colorLiteralRed: 0.297, green: 0.7, blue: 0.395, alpha: 1)
      }
      scoreLabel.text = String(score)
    }
    else {
      return false
    }
    
    emptyCardView.isHidden = true
    cardView.isHidden = false
    
    return true
  }
  
  
  
  // SIDE MENU FUNCTIONS
  
  @IBAction func showSideMenuTapped(_ sender: Any) {
    if sideMenuOpen != true {
      showSideMenu()
    }
    else{
      hideSideMenu()
    }
  }
  
  // Hides the side menu, if tapped outside the side menu
  @IBAction func screenCoverTapped(_ sender: Any) {
    hideSideMenu()
  }
  
  // Puts an overlay button to the content and slides out side menu
  func showSideMenu(){
    sideMenuOpen = true
    UIView.animate(withDuration: 0.4, animations: {
      self.sideMenuTrailingConstriant.constant = 0
      self.screenCoverButton.alpha = 1
      self.view.layoutIfNeeded()
    })
  }
  
  // Removes overlay button to the content and slides side menu off screen
  func hideSideMenu(){
    sideMenuOpen = false
    UIView.animate(withDuration: 0.4, animations: {
      self.sideMenuTrailingConstriant.constant = -self.sideMenuView.frame.width
      self.screenCoverButton.alpha = 0
      self.view.layoutIfNeeded()
    })
  }
  
  // Restart Selection Process
  @IBAction func restartArena(_ sender: Any) {
    
    // Action sheet for restart confirmation
    let actionSheet = UIAlertController(title: "Restart Arena Selection",
                                        message: "Do you wish to reset the deck?",
                                        preferredStyle: .actionSheet)
    
    
    // Confirmation Action
    actionSheet.addAction(UIAlertAction(title: "Reset",
                                        style: .destructive,
                                        handler: {(action:UIAlertAction) in
                                          self.navigationController?.popToRootViewController(animated: true)
    }))
    
    // Cancel Action
    actionSheet.addAction(UIAlertAction(title: "Cancel",
                                        style: .cancel,
                                        handler: nil))
    
    // Puts the action sheet on the screen
    self.present(actionSheet, animated: true, completion: nil)
  }
  
  // Save Deck
  @IBAction func saveDeck(_ sender: Any) {
    
    // Setup the Alert
    let alert = UIAlertController (title: "Save Deck",
                                   message: "Enter a Custom Deck Name",
                                   preferredStyle: .alert)
    // Add text field
    alert.addTextField{(textField: UITextField) in
      textField.placeholder = self.currentDeck?.deckName
      textField.textColor = UIColor.blue
      textField.clearButtonMode = UITextFieldViewMode.whileEditing
    }
    
    //Add the ok Button
    alert.addAction(UIAlertAction(title: "OK",
                                  style: .default,
                                  handler: {(action:UIAlertAction) in
                                    
                                    if let textField = alert.textFields?.first{
                                      if textField.text != ""{
                                        self.currentDeck?.setName(textField.text!)
                                      }
                                      self.navigationController?.popToRootViewController(animated: true)
                                    }
    }))
    
    // Puts the action sheet on the screen
    self.present(alert, animated: true, completion: nil)
    
  }    
  
  
  
  // SEGUE FUNCTIONS
  
  // Prepare for Seque
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    // Send Class name to the Card Selection Controller
    if segue.destination is CardSelectionTableViewController{
      let vc = segue.destination as! CardSelectionTableViewController
      vc.currentClass = (currentDeck?.deckHeroClass)!
      activePair = sender as! [UIView]
    }
    
    
    // Send deck to the Deck Details view
    if segue.destination is DeckDetailsViewController {
      let detailsVC = segue.destination as! DeckDetailsViewController
    
      let destinationTitle = currentDeck?.deckName
      detailsVC.title = destinationTitle
      detailsVC.deckName = destinationTitle
      detailsVC.deck = currentDeck
    }
    
    // Send deck to the Mana Curve view
    if segue.destination is ManaCurveViewController {
      let manaVC = segue.destination as! ManaCurveViewController
      if currentDeck != nil{
        manaVC.deck = DeckManaCurve(deck: currentDeck!)
      }
    }
    
  }
  
  
  // Unwind Segue from the Card Selection Table View Controller
  @IBAction func unwindToArena(segue: UIStoryboardSegue) {
    
    // Set the selected card to the active pair
    if let cardSelectVC = segue.source as? CardSelectionTableViewController{
      
      if cardSelectVC.selectedCard != nil {
        if populateCardDetailsToView(cardView: activePair[0],
                                     emptyCardView: activePair[1],
                                     card: cardSelectVC.selectedCard!){
          if activeCard == "top"{
            topCard = cardSelectVC.selectedCard!
          }
          else if activeCard == "middle"{
            middleCard = cardSelectVC.selectedCard!
          }
          else if activeCard == "bottom"{
            bottomCard = cardSelectVC.selectedCard!
          }
          else{
            print("Could Not Find Active Card")
          }
        }
        else{
          print("Card did not load")
        }
      }
    }
  }
  
}

extension ArenaSelectionViewController: UITabBarControllerDelegate{
  
  //To prevent the user from accidentally reseting their arena selection process and making a new deck
  //This function will stop the "Arena" tab from taking the user back to the hero selection screen
  
  func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    
    let destVC = viewController as! UINavigationController
    
    if destVC.viewControllers[0] is HeroSelectionViewController && tabBarController.selectedIndex == 0{      
      return false
    }
    
    return true
    
  }
  
}
