//
//  ManaCurveViewController.swift
//  arena_forge
//
//  Created by Paul Weston on 11/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import UIKit

class ManaCurveViewController: UIViewController {

  var deck: DeckManaCurve?
  
  //Mana Labels
  @IBOutlet weak var minionLabel: UILabel!
  @IBOutlet weak var spellLabel: UILabel!
  @IBOutlet weak var weaponsLabel: UILabel!
  @IBOutlet weak var battlecryLabel: UILabel!
  @IBOutlet weak var overloadLabel: UILabel!
  @IBOutlet weak var tauntLabel: UILabel!
  @IBOutlet weak var deathrattleLabel: UILabel!
  
  //Mana Bar Objects
  @IBOutlet weak var mana0TopConstraint: NSLayoutConstraint!
  @IBOutlet weak var mana0View: UIView!
  @IBOutlet weak var mana0Label: UILabel!
  
  @IBOutlet weak var mana1TopConstraint: NSLayoutConstraint!
  @IBOutlet weak var mana1View: UIView!
  @IBOutlet weak var mana1Label: UILabel!
  
  @IBOutlet weak var mana2TopConstraint: NSLayoutConstraint!
  @IBOutlet weak var mana2View: UIView!
  @IBOutlet weak var mana2Label: UILabel!
  
  @IBOutlet weak var mana3TopConstraint: NSLayoutConstraint!
  @IBOutlet weak var mana3View: UIView!
  @IBOutlet weak var mana3Label: UILabel!
  
  @IBOutlet weak var mana4TopConstraint: NSLayoutConstraint!
  @IBOutlet weak var mana4View: UIView!
  @IBOutlet weak var mana4Label: UILabel!
  
  @IBOutlet weak var mana5TopConstraint: NSLayoutConstraint!
  @IBOutlet weak var mana5View: UIView!
  @IBOutlet weak var mana5Label: UILabel!
  
  @IBOutlet weak var mana6TopConstraint: NSLayoutConstraint!
  @IBOutlet weak var mana6View: UIView!
  @IBOutlet weak var mana6Label: UILabel!
  
  @IBOutlet weak var mana7TopConstraint: NSLayoutConstraint!
  @IBOutlet weak var mana7View: UIView!
  @IBOutlet weak var mana7Label: UILabel!
  
  
  
  // Lifecycle method for performing tasks after the view has loaded
  override func viewDidLoad() {
    super.viewDidLoad()
    populateTypeLabels()
    populateManaCurve()
  }
  
  
  // Populate the Types Labels
  private func populateTypeLabels(){
    minionLabel.text = deck?.getTypeValue("Minions")
    spellLabel.text = deck?.getTypeValue("Spells")
    weaponsLabel.text = deck?.getTypeValue("Weapons")
    battlecryLabel.text = deck?.getTypeValue("Battlecry")
    overloadLabel.text = deck?.getTypeValue("Overload")
    tauntLabel.text = deck?.getTypeValue("Taunt")
    deathrattleLabel.text = deck?.getTypeValue("DeathRattle")
  }
  
  //Updates All Mana Bars
  func populateManaCurve(){
    updateManaBar(constraint: mana0TopConstraint,
                  view: mana0View,
                  label: mana0Label,
                  manaIndex: 0)
    updateManaBar(constraint: mana1TopConstraint,
                  view: mana1View,
                  label: mana1Label,
                  manaIndex: 1)
    updateManaBar(constraint: mana2TopConstraint,
                  view: mana2View,
                  label: mana2Label,
                  manaIndex: 2)
    updateManaBar(constraint: mana3TopConstraint,
                  view: mana3View,
                  label: mana3Label,
                  manaIndex: 3)
    updateManaBar(constraint: mana4TopConstraint,
                  view: mana4View,
                  label: mana4Label,
                  manaIndex: 4)
    updateManaBar(constraint: mana5TopConstraint,
                  view: mana5View,
                  label: mana5Label,
                  manaIndex: 5)
    updateManaBar(constraint: mana6TopConstraint,
                  view: mana6View,
                  label: mana6Label,
                  manaIndex: 6)
    updateManaBar(constraint: mana7TopConstraint,
                  view: mana7View,
                  label: mana7Label,
                  manaIndex: 7)
  }
  
  //Updates a Single Mana Bar
  func updateManaBar(constraint: NSLayoutConstraint, view: UIView,
                     label: UILabel, manaIndex: Int){
    
    if let deck = self.deck{
  
      if deck.manaValues[manaIndex] > 0 {
      
        var manaValue = deck.manaValues[manaIndex]
        label.text = String(manaValue)
      
        if manaValue > 10{
          manaValue = 10
        }
      
        constraint.constant = CGFloat(
        view.frame.height - ((view.frame.height/CGFloat(10)) * CGFloat(manaValue) - constraint.constant))
    
      }
      else{
        constraint.constant = CGFloat(view.frame.height)
      }
    }
  }
  
  override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
  }
  
}
