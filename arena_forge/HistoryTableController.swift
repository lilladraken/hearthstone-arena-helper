//
//  HistoryTableController.swift
//  arena_forge
//
//  Created by Steph Ridnell on 28/12/17.
//  Copyright © 2017 Taylor Swift. All rights reserved.
//

import Foundation
import UIKit


class HistoryTableController: UIViewController, UITableViewDataSource, UISearchResultsUpdating {
  
  // Reference to table view outlet
  @IBOutlet weak var tableView: UITableView!
  
  // Dummy data
  var decks: [DeckCD]? = []
  var filteredDecks: [DeckCD]? = []
  
  // Search controller
  let searchController = UISearchController(searchResultsController: nil)
  
  private var continueDeckBuild = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = "History"
  
    tableView.dataSource = self
    
    if !Deck.sharedInstance.noDecksAvailable{
      decks = Deck.sharedInstance.getUserDecks()!
      filteredDecks = decks
    }
    
    // Sets up this class as the Search controller and creates the search bar 
    // at the top of the table view.
    searchController.searchResultsUpdater = self
    searchController.dimsBackgroundDuringPresentation = false
    self.definesPresentationContext = true
    
    tableView.tableHeaderView = searchController.searchBar
  }
  
  
  
  // Update Table when returning to History View
  override func viewDidAppear(_ animated: Bool) {
    
    if !Deck.sharedInstance.noDecksAvailable{
      decks = Deck.sharedInstance.getUserDecks()!
      filteredDecks = decks
    }

    tableView.reloadData()
    
    if continueDeckBuild{
      continueBuild()
    }
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    self.searchController.isActive = false
  }
  
  
  // Set number of sections for the table
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  
  // Detemine number of rows
  // We only have one section - so do not need to use the variable 'section'
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if searchController.isActive && searchController.searchBar.text != "" {
        return filteredDecks!.count
    }
    return decks!.count
  }
  
  
  // Setup Cells in Table View
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    // Create the cell by using a method of UITableView
    let deckCell = tableView.dequeueReusableCell(withIdentifier: "deckCell", for: indexPath)
    
    // Look up the data item that corresponds to the indexPath.
    let deckIndex: Dictionary = changeDataSource(indexPath: indexPath as NSIndexPath)
    let deck = deckIndex["name"] as! DeckCD
  
    // Set cell settings
    deckCell.textLabel?.text = deck.deckName
    deckCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
    deckCell.textLabel?.textColor = UIColor.white
    
    return deckCell
  }
  
  
  // Override to support conditional editing of the table view.
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
  }
  
  
  
  //Deleting a deck.
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    
    let deck: DeckCD
    
    if searchController.isActive && searchController.searchBar.text != "" {
      deck = (filteredDecks?[indexPath.item])!
      filteredDecks?.remove(at: indexPath.row)
    } else {
      deck = (decks?[indexPath.item])!
      decks?.remove(at: indexPath.row)
    }
    
    // Finds the Arena Navigation controller via the tab controller
    let destNC = self.tabBarController?.viewControllers?[0] as! UINavigationController!
    
    // Once in the hero view controller pass the deck to be forwarded to the Arena
    if destNC!.viewControllers.count > 1{
      if let heroVC = destNC?.viewControllers[1] as? ArenaSelectionViewController{
        if deck == heroVC.currentDeck!{
          destNC?.popToRootViewController(animated: false)
        }
      }
    }
    Deck.sharedInstance.deleteDeck(deck: deck)
    
    if !Deck.sharedInstance.noDecksAvailable{
      decks = Deck.sharedInstance.getUserDecks()!
    }
    else{
      decks = []
    }
    
    self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
  }
  
  
  // Check to see if we should be displaying data from the filtered list or the main data source.
  func changeDataSource( indexPath: NSIndexPath) -> Dictionary<String, Any> {
    var deck: Dictionary<String, Any>
    if searchController.isActive && searchController.searchBar.text != "" {
      deck = ["name": filteredDecks![indexPath.row]]
    } else {
      deck = ["name": decks![indexPath.row]]
    }
    return deck
  }
  
  
  // Method implemented by the protocol that is called when the search bar is being used for searching.
  func updateSearchResults(for: UISearchController) {
    filterContentForSearchText(searchText: searchController.searchBar.text!)
  }
  
  
  // Searches the main data source for the search text to return a filtered result.
  func filterContentForSearchText(searchText: String, scope: String = "All") {
    filteredDecks = decks?.filter { deck in
      return (deck.deckName?.lowercased().contains(searchText.lowercased()))!
    }
    
    // reload the table data to display the filtered results.
    tableView.reloadData()
  }
  
  
  // Prepare for segue method
  override func prepare (for segue: UIStoryboardSegue, sender: Any?) {
    // Get the deck that was selected
    let indexPath = self.tableView.indexPathForSelectedRow!
    let deck: DeckCD
    
    if searchController.isActive && searchController.searchBar.text != "" {
      deck = (filteredDecks?[indexPath.item])!
    } else {
      deck = (decks?[indexPath.item])!
    }
    
    // Set a property on the destination view controller
    let detailsVC = segue.destination as! DeckDetailsViewController
    
    let destinationTitle = deck.deckName
    detailsVC.title = destinationTitle
    detailsVC.deckName = destinationTitle
    detailsVC.deck = deck
  }
  
  
  // Unwind Function
  @IBAction func unwindToHistory(segue: UIStoryboardSegue) {
    
    // Unwinds from the Deck Detail view for the sending to the Arena NavigationController
    if segue.source is DeckDetailsViewController{
      
      let sourceVC = segue.source as? DeckDetailsViewController
      
      // Finds the Arena Navigation controller via the tab controller
      let destNC = self.tabBarController?.viewControllers?[0] as! UINavigationController!
      
      // Once in the hero view controller pass the deck to be forwarded to the Arena
      let heroVC = destNC?.viewControllers[0] as! HeroSelectionViewController
      if let deck = sourceVC?.deck{
        heroVC.setRebuildDeck(deck: deck)
      }
      
      continueDeckBuild = true
      
    }
  }
  
  // Pass the user to the Arena Navigation Controller
  private func continueBuild(){
    self.continueDeckBuild = false
    
    // Switch to the Arena Tab
    self.tabBarController?.selectedIndex = 0
    
    // Finds the Arena Navigation controller via the tab controller
    let destNC = self.tabBarController?.viewControllers?[0] as! UINavigationController!
    
    // Pops the navigation controller back to the root controller
    destNC?.popToRootViewController(animated: false)
  }
  
  //Deinits Search Controller before the View Controller is deinitialized
  deinit {
    self.searchController.view.removeFromSuperview()
  }
  
}


