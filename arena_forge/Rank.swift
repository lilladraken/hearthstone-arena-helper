//
//  Rank.swift
//  arena_forge
//
//  Created by Paul Weston on 9/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import Foundation

class Rank {
  
  // Static Variable for all rank sets
  static var allRanks: [Rank] = []
  
  // Rank Variables
  let name: String
  let druidRank: Int
  let hunterRank: Int
  let warlockRank: Int
  let shamanRank: Int
  let rogueRank: Int
  let priestRank: Int
  let paladinRank: Int
  let warriorRank: Int
  let mageRank: Int
  

  init(name: String, druidRank: Int, hunterRank: Int, warlockRank: Int,
       shamanRank: Int, rogueRank: Int, priestRank: Int,
       paladinRank: Int, warriorRank: Int, mageRank: Int ){
  
    self.name = name
    self.druidRank = druidRank
    self.hunterRank = hunterRank
    self.warlockRank = warlockRank
    self.shamanRank = shamanRank
    self.rogueRank = rogueRank
    self.priestRank = priestRank
    self.paladinRank = paladinRank
    self.warriorRank = warriorRank
    self.mageRank = mageRank
    
    Rank.allRanks.append(self)
    
  }
  
  // STATIC FUNCTIONS
  
  // Returns a rank if it matches the supplied name
  static func getRankByName(name: String) -> Rank?{
    
    for rank in allRanks{
      if name.lowercased() == rank.name.lowercased(){
        return rank
      }
    }
    return nil
  
  }



}
