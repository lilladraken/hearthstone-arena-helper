//
//  DeckDetailsViewController.swift
//  arena_forge
//
//  Created by Steph Ridnell on 31/12/17.
//  Copyright © 2017 Taylor Swift. All rights reserved.
//

import UIKit

class DeckDetailsViewController: UIViewController, UITableViewDataSource {
  
  // Reference to table view outlet
  @IBOutlet weak var deckDetailsTable: UITableView!
  
  // Continue Build Button Reference
  @IBOutlet weak var continueBuildButton: UIBarButtonItem!
  
  // Current Deck Details
  var deck: DeckCD?
  var deckName:String?
  var cards:[CardCD] = []
  
  // Lifecycle method for performing tasks after the view has loaded
  override func viewDidLoad() {
    super.viewDidLoad()
    self.tabBarController?.delegate = self
    
    // Gets deck cards
    cards = (deck?.getSortedCards())!
    deckDetailsTable.dataSource = self
  }
  
  // Function called before the view appears
  override func viewWillAppear(_ animated: Bool) {
    // Refresh Tabbar delegate
    self.tabBarController?.delegate = self
    
    // Enable the continue building button if the deck is full or
    // user is currently building that deck
    if let deck = self.deck{
      if !deck.isDeckFull && self.tabBarController?.selectedIndex != 0{
        self.continueBuildButton.isEnabled = true
      }
    }
    
  }
  
  
  // Set number of sections for the table
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  
  // Detemine number of rows
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cards.count
  }
  
  // Set up table view cells
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    // Create the cell by using a method of UITableView
    let cardCell = tableView.dequeueReusableCell(withIdentifier: "cardCell", for: indexPath) as! DeckDetailsViewCell
    
    // Set cell settings
    let card = cards[indexPath.item]
    DispatchQueue.main.async(execute: {cardCell.cardImage?.image = card.getImage()})
    cardCell.cardLabel?.text = card.name
    cardCell.manaCost?.text = String(card.cost)
    cardCell.attackVal?.text = String(card.attack)
    cardCell.healthVal?.text = String(card.health)
    cardCell.cardLabel?.textColor = UIColor.white
    
    return cardCell
  }
  
  
  // Lifecycle method for clearing up memory resources
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  // Prepare for segue method
  override func prepare (for segue: UIStoryboardSegue, sender: Any?) {
    
    // Set a property on the destination view controller
    let destinationVC = segue.destination
    
    // Segue to Card Detail View
    if destinationVC is CardDetailsViewController{
      let destinationVC = segue.destination as! CardDetailsViewController
      
      // Get the card that was selected
      let indexPath = deckDetailsTable .indexPathForSelectedRow!
      let card = cards[indexPath.item]
      
      
      let destinationTitle = card.name
      destinationVC.title = destinationTitle
      destinationVC.card = card
    }
    
    // Segue to Arena View
    if destinationVC is ArenaSelectionViewController{
      let destinationVC = segue.destination as! ArenaSelectionViewController
      
      // Sets the arena deck to the currently view deck
      destinationVC.currentDeck = deck
    }
    
  }
    
}

// Prototype Deck Cell Class
class DeckDetailsViewCell: UITableViewCell {
  
  @IBOutlet weak var cardImage: UIImageView!
  @IBOutlet weak var cardLabel: UILabel!
  @IBOutlet weak var manaCost: UILabel!
  @IBOutlet weak var attackVal: UILabel!
  @IBOutlet weak var healthVal: UILabel!
  
}


extension DeckDetailsViewController: UITabBarControllerDelegate{
  
  //To prevent the user from accidentally reseting their arena selection process and making a new deck
  //This function will stop the "Arena" tab from taking the user back to the hero selection screen
  
  func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    
    let destVC = viewController as! UINavigationController
    
    if destVC.viewControllers[0] is HeroSelectionViewController && tabBarController.selectedIndex == 0{
      return false
    }
    
    return true
    
  }
  
}

