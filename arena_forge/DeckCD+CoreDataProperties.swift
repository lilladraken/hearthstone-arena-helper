//
//  DeckCD+CoreDataProperties.swift
//  arena_forge
//
//  Created by Paul Weston on 26/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import Foundation
import CoreData


extension DeckCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DeckCD> {
        return NSFetchRequest<DeckCD>(entityName: "DeckCD")
    }

    @NSManaged public var deckName: String?
    @NSManaged public var deckDate: NSDate?
    @NSManaged public var deckHeroClass: String?
    @NSManaged public var deckCardIDs: [Int32]?
    @NSManaged public var userId: Int64

}
