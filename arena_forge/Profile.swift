//
//  Profile.swift
//  arena_forge
//
//  Created by Paul Weston on 27/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Profile{
  
  // Get a reference to your App Delegate
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
  
  // Get a database context from the app delegate
  var managedContext: NSManagedObjectContext
  {
    get {
      return appDelegate.persistentContainer.viewContext
    }
  }
  
  var profiles = [NSManagedObject]()
  var currentProfile: UserProfile?
  
  // Load all Profile Data from Core Data
  private func getProfileFromCoreData()
  {
    do
    {
      let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"UserProfile")
      
      let results =
        try managedContext.fetch(fetchRequest)
      profiles = results as! [NSManagedObject]
    }
    catch let error as NSError
    {
      print("Could not fetch \(error), \(error.userInfo)")
    }
  }
  
  // Save/Update Profile Data in Code Data
  func saveProfile(userName: String, email: String, password: String, favHero: String, image: UIImage?, existing: UserProfile?)  {
    
    let entity = NSEntityDescription.entity(forEntityName: "UserProfile",in: managedContext)!
    
    // If existing is not nil then update existing movie
    if let _ = existing
    {
      existing!.userName = userName
      existing!.email = email
      existing!.favHero = favHero
      existing!.password = password
      if let _ = image{
        let imageData = UIImagePNGRepresentation(image!)! as NSData
        existing!.image = imageData
      }
    }
      // Create a new Profile object and update it with the data passed in from the View Controller
    else
    {
      // Create new Profile
      let profile = NSManagedObject(entity: entity,insertInto: managedContext)
      
      //Generate UserID
      let date = Date()
      let generatedId = date.timeIntervalSince1970      
      let userId = Int64(generatedId)
      
      profile.setValue(userId, forKeyPath: "userId")
      profile.setValue(userName, forKeyPath: "userName")
      profile.setValue(email, forKeyPath: "email")
      profile.setValue(password, forKeyPath: "password")
      profile.setValue(favHero, forKeyPath: "favHero")
      if let _ = image{
        let imageData = UIImagePNGRepresentation(image!)! as NSData
        profile.setValue(imageData, forKeyPath: "image")
      }
      profiles.append(profile)
    }
    updateDatabase()
  }
  
  // Delete User
  func deleteUser(user: UserProfile)
  {
    for (index,element) in profiles.enumerated(){
      if let loopProfile = element as? UserProfile{
        if user.userId == loopProfile.userId{
          profiles.remove(at: index)
        }
      }
    }
    managedContext.delete(user)
    updateDatabase()
  }
  
  // Save the current state of the objects in the managed context into the database.
  func updateDatabase()
  {
    do
    {
      try managedContext.save()
    }
    catch let error as NSError
    {
      print("Could not save \(error), \(error.userInfo)")
    }
  }
  
  // Singleton Functions
  private init()
  {
    getProfileFromCoreData()
  }
  
  fileprivate struct Static
  {
    static var instance: Profile?
  }
  
  
  class var sharedInstance: Profile
  {
    if (Static.instance == nil)
    {
      Static.instance = Profile()
    }
    return Static.instance!
  }
  
  // Check if username already exists
  func doesUsernameExist(username: String) -> Bool{
    
    for profile in profiles{
      if let p = profile as? UserProfile{
        if p.userName == username{
          return true
        }
      }
    }
    
    return false
  }
  
  // Returns User Profile
  func getProfileByUsername(username: String) -> UserProfile?{
    
    for profile in profiles{
      if let p = profile as? UserProfile{
        if p.userName == username{
          return p
        }
      }
    }
    return nil
  }
  
  // Set the Current UserProfile
  func setCurrentUserProfile(user: UserProfile){
    currentProfile = user
  }
  
}
