//
//  ProfileViewController.swift
//  arena_forge
//
//  Created by Paul Weston on 28/12/17.
//  Copyright © 2017 Taylor Swift. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  // Current User Profile
  var currentProfile: UserProfile!
  
  // Profile Image
  @IBOutlet weak var profileImageView: UIImageView!
  
  // Camera Button View
  @IBOutlet weak var cameraButtonView: UIView!
  
  // Scrollview
  @IBOutlet weak var scrollView: UIScrollView!
  var didOffsetScrollView: Bool = false
  
  // Profile information Text Fields
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var usernameTextField: UITextField!
  
  // Hero Selection Button
  @IBOutlet weak var favHeroSelect: UIButton!
  
  //Variables for handling the Keyboard Offset
  var keyboardHeight: CGFloat = 0.0
  var currentTextField: UITextField!
  var previousContentOffset: CGPoint!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Set Current Profile
    currentProfile = Profile.sharedInstance.currentProfile
    
    // Make Profile Image a Circle
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2
    self.profileImageView.clipsToBounds = true
    
    // Crop Camera Button into Circle
    self.cameraButtonView.layer.cornerRadius = self.cameraButtonView.frame.size.width/2
    self.cameraButtonView.clipsToBounds = true
    
    // Keyboard Dismissal when tapping outside keyboard
    self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view,
                                                          action: #selector(UIView.endEditing(_:))))
       loadUserInfo()
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    // Set Observers
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),
                                           name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)),
                                           name: NSNotification.Name.UIKeyboardWillHide, object: nil)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    // Remove Observers
    NotificationCenter.default.removeObserver(self)
  }
  
  
  // Load User Info
  func loadUserInfo(){
    if currentProfile != nil{
      emailTextField.text = currentProfile.email
      usernameTextField.text = currentProfile.userName
      if currentProfile.favHero != ""{
        favHeroSelect.setTitle(currentProfile.favHero, for: .normal)
      }
      else{
        favHeroSelect.setTitle("Select Hero", for: .normal)
      }
    
      if let image = currentProfile.image{
        profileImageView.image = UIImage(data: image as Data)
      }
    }
  }
  
  // Camera Button Pressed
  @IBAction func cameraButtonPress(_ sender: Any) {
    
    // Image picker controller for handling image selection from camera or image library
    let imagePickerController = UIImagePickerController()
    
    // There is a Objective C runtime warning here because of a bug in xcode 8,
    // Apple's statement is to just ignore it, will ask Rodney about it
    imagePickerController.delegate = self
    
    // Action sheet for photo source selection
    let actionSheet = UIAlertController(title: "Profile Image Source",
                                        message: "Choose image source",
                                        preferredStyle: .actionSheet)
    
    // Camera Action
    actionSheet.addAction(UIAlertAction(title: "Camera",
                                        style: .default,
                                        handler: {(action:UIAlertAction) in
                                          
                                          //Check if the Camera is Available
                                          if UIImagePickerController.isSourceTypeAvailable(.camera){
                                            imagePickerController.sourceType = .camera
                                            self.present(imagePickerController, animated: true, completion: nil)
                                          }
                                          else{
                                            imagePickerController.sourceType = .photoLibrary
                                            self.present(imagePickerController, animated: true, completion: nil)
                                          }
    }))
    
    
    // Pick Photo from the library on the device
    actionSheet.addAction(UIAlertAction(title: "Photo Library",
                                        style: .default,
                                        handler: {(action:UIAlertAction) in
                                          imagePickerController.sourceType = .photoLibrary
                                          self.present(imagePickerController, animated: true, completion: nil)
    }))
    
    
    // Cancel Action
    actionSheet.addAction(UIAlertAction(title: "Cancel",
                                        style: .cancel,
                                        handler: nil))
    
    // Puts the action sheet on the screen
    self.present(actionSheet, animated: true, completion: nil)
    
  }
  
  
  
  // Changes the user Profile image to the one selected in the picker controller
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]){
    if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
      profileImageView.image = image
      Profile.sharedInstance.saveProfile(userName: currentProfile.userName!,
                                         email: currentProfile.email!,
                                         password: currentProfile.password!,
                                         favHero: currentProfile.favHero!,
                                         image: image,
                                         existing: currentProfile)
    } else{
      print("Something went wrong")
    }
    picker.dismiss(animated: true, completion: nil)
  }
  
  
  // Handles when a user cancels image selection
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated:true, completion:nil)
  }
  
  
  // Keyboard is dismissed with return button
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    textField.resignFirstResponder()
    
    return true
  }
  
  
  // When the User Starts typing into a text field
  func textFieldDidBeginEditing(_ textField: UITextField) {
    
    currentTextField = textField
    
  }
  
  
  // Functions called by the observers when the keyboard appears on the screen
  // offsetting the scrollview and returning it back in place
  func keyboardWillHide(notification: Notification) {
    
    // Shifts View back down when entry ends
    if(didOffsetScrollView){
      scrollView.setContentOffset(previousContentOffset, animated: true)
      didOffsetScrollView = false
    }
    
  }
  
  
  func keyboardWillShow(notification: Notification) {
    let userInfo:NSDictionary = notification.userInfo! as NSDictionary
    let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
    keyboardHeight = keyboardFrame.cgRectValue.height
    
    // Shifts View up if Text Field falls behind keyboard frame
    let textFieldPosition = currentTextField.convert(currentTextField.frame.origin, to: nil)
    let screenHeight: CGFloat = UIScreen.main.bounds.height
    
    if(screenHeight - textFieldPosition.y < keyboardHeight){
      let screenOffset = keyboardHeight - (screenHeight - (textFieldPosition.y + currentTextField.frame.height))
      
      previousContentOffset = scrollView.contentOffset
      
      scrollView.setContentOffset(CGPoint(x:0, y:screenOffset), animated: true)
      didOffsetScrollView = true
    }
    
  }
  
  
  // Unwind Seque from the Fav Hero Selection Table View Controller
  @IBAction func unwindToProfile(segue: UIStoryboardSegue) {
    
    // Sets the name of the Fav Hero
    if let favHeroVC = segue.source as? FavHeroTableViewController{
      if favHeroVC.selectedHero != "" {
        self.favHeroSelect.setTitle(favHeroVC.selectedHero, for: .normal)
        Profile.sharedInstance.saveProfile(userName: currentProfile.userName!,
                                           email: currentProfile.email!,
                                           password: currentProfile.password!,
                                           favHero: favHeroVC.selectedHero!,
                                           image: nil,
                                           existing: currentProfile)
      }
    }
  }
  
  
  // Updates the user information when text fields have been edited
  func textFieldDidEndEditing(_ textField: UITextField) {
    if textField == usernameTextField{
      // If Username already existing
      if let _ = Profile.sharedInstance.getProfileByUsername(username: usernameTextField.text!){
        if usernameTextField.text != currentProfile.userName{
          // Alert for when the wrong user information is entered
          profileExistsAlert()
          usernameTextField.text = currentProfile.userName
        }
      }
      else{
        Profile.sharedInstance.saveProfile(userName: usernameTextField.text!,
                                           email: currentProfile.email!,
                                           password: currentProfile.password!,
                                           favHero: currentProfile.favHero!,
                                           image: nil,
                                           existing: currentProfile)
      
      }
    }
    if textField == emailTextField{
      Profile.sharedInstance.saveProfile(userName: currentProfile.userName!,
                                         email: emailTextField.text!,
                                         password: currentProfile.password!,
                                         favHero: currentProfile.favHero!,
                                         image: nil,
                                         existing: currentProfile)
    }
  }
  
  // Alert for when the wrong user information is entered
  func profileExistsAlert(){
    
    let alert = UIAlertController(title: "Username Already Exists", message: "oh no, someone else has that username, sorry for your loss", preferredStyle: UIAlertControllerStyle.alert)
    
    let cancelAction = UIAlertAction(title: "Try Again",
                                     style: .cancel,
                                     handler: nil)
    
    alert.addAction(cancelAction)
    self.present(alert, animated: true)
  }
  
  // Logout of profile
  @IBAction func logout(_ sender: Any) {
    
    // Action sheet for logout confirmation
    let actionSheet = UIAlertController(title: "Log Out",
                                        message: "Are you sure you want to Log Out?",
                                        preferredStyle: .actionSheet)
    
    
    // Confirmation Action
    actionSheet.addAction(UIAlertAction(title: "Log Out",
                                        style: .destructive,
                                        handler: {(action:UIAlertAction) in
                                          self.performSegue(withIdentifier: "logout", sender: nil)
    }))
    
    // Cancel Action
    actionSheet.addAction(UIAlertAction(title: "Cancel",
                                        style: .cancel,
                                        handler: nil))
    
    // Puts the action sheet on the screen
    self.present(actionSheet, animated: true, completion: nil)
  }
  
}
