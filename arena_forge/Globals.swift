//
//  Global.swift
//  arena_forge
//
//  Created by Paul Weston on 11/2/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

// Global notification string for card load finish
let cardLoadFinishedNotificationKey = "com.stephridnell.tswift.arena-forge.cardLoadFinished"
