//
//  CardDetailsViewController.swift
//  arena_forge
//
//  Created by Steph Ridnell on 7/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import UIKit

class CardDetailsViewController: UIViewController {
  
  // Card (passed in from table controller)
  var card: CardCD?
  
  //Scrollview
  @IBOutlet weak var scrollView: UIScrollView!
  var didOffsetScrollView: Bool = false
  
  // Image
  @IBOutlet weak var cardImage: UIImageView!
  
  // Flavor (text under image)
  @IBOutlet weak var flavorText: UILabel!
  
  // Labels
  @IBOutlet weak var typeText: UILabel!
  @IBOutlet weak var classText: UILabel!
  @IBOutlet weak var rarityText: UILabel!
  @IBOutlet weak var costText: UILabel!
  @IBOutlet weak var attackText: UILabel!
  @IBOutlet weak var healthText: UILabel!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.tabBarController?.delegate = self
    
    // Set image
    if let card = self.card{
      DispatchQueue.main.async(execute: {self.cardImage?.image = card.getImage()})
      // Set flavour
      flavorText?.text = "\"" + card.getFlavor() + "\""
      
      // Set detail labels
      typeText?.text = card.type
      classText?.text = card.cardClass
      rarityText?.text = card.rarity
      costText?.text = String(card.cost)
      attackText?.text = String(card.attack)
      healthText?.text = String(card.health)
    }
    else{
      self.cardImage?.image = UIImage(named:"card-back")
    }

  }
  
  // Refresh Tabbar delegate
  override func viewDidAppear(_ animated: Bool) {
    self.tabBarController?.delegate = self
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
}


extension CardDetailsViewController: UITabBarControllerDelegate{
  
  //To prevent the user from accidentally reseting their arena selection process and making a new deck
  //This function will stop the "Arena" tab from taking the user back to the hero selection screen
  
  func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    
    let destVC = viewController as! UINavigationController
    
    if destVC.viewControllers[0] is HeroSelectionViewController &&
      //self.isViewLoaded &&
      //(self.view.window != nil) &&
      tabBarController.selectedIndex == 0{
      
      return false
    }
    
    return true
    
  }
  
}
