//
//  CardCD+CoreDataClass.swift
//  arena_forge
//
//  Created by Paul Weston on 28/1/18.
//  Copyright © 2018 Taylor Swift. All rights reserved.
//

import Foundation
import CoreData
import UIKit


@objc(CardCD)
public class CardCD: NSManagedObject {
  
  var rank: Rank{
    if let rank = Rank.getRankByName(name: name!){
      return rank
    }
    else{
      return Rank.getRankByName(name: "Blank")!
    }
  }

  // Return the rank value based on the class if the rank value is 0 then the rank value is calculated
  func getRank(heroClass: String) -> Int{
    var rankValue = 0
  
    if heroClass.lowercased() == "druid"{
      rankValue = rank.druidRank
    }
    else if heroClass.lowercased() == "hunter"{
      rankValue = rank.hunterRank
    }
    else if heroClass.lowercased() == "warlock"{
      rankValue = rank.warlockRank
    }
    else if heroClass.lowercased() == "shaman"{
      rankValue = rank.shamanRank
    }
    else if heroClass.lowercased() == "rogue"{
      rankValue = rank.rogueRank
    }
    else if heroClass.lowercased() == "priest"{
      rankValue = rank.priestRank
    }
    else if heroClass.lowercased() == "paladin"{
      rankValue = rank.paladinRank
    }
    else if heroClass.lowercased() == "warrior"{
      rankValue = rank.warriorRank
    }
    else if heroClass.lowercased() == "mage"{
      rankValue = rank.mageRank
    }
  
    if(rankValue == 0){
        rankValue = calculateRank()
    }
  
    return rankValue
  
  }
  
  
  // Calculates the Rank Value based on the Cards Stats
  private func calculateRank() -> Int{
    var rankSum = 0
  
    // Rank Calculation based on the compareable values of other cards
    // Feel free to rewrite or add to the Calculation
    rankSum += Int(self.health) * 4
    rankSum += Int(self.cost) * 4
    rankSum += Int(self.attack) * 4
  
    return rankSum
  
  }
  
  // Returns the Card Image based on the URL, should be called Async
  func getImage() -> UIImage{
    let imageURL = URL(string: self.imageURL!)
  
    if let imageData = try? Data(contentsOf: imageURL!){
      let image = UIImage(data: imageData)
      return image!
    }
      
    return UIImage(named: "card-back")!
  }
  
  // Getter for text
  func getText()->String{
    if self.text != nil{
      return self.text!
    }
    else{
      return ""
    }
  }
  
  // Getter for type
  func getType()->String{
    if self.type != nil{
      return self.type!
    }
    else{
      return ""
    }
  }
  
  // Getter for Flavor
  func getFlavor() -> String{
    if self.flavor != nil{
      return self.flavor!.removeSpecialChars()
    }
    else{
      return ""
    }
  }
  
  
}
